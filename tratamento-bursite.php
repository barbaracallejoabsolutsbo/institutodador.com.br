<?php
$title       = "Tratamento Bursite";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Esse tipo de inflamação, que costuma ocorrer não só nos ombros, pode ser diagnosticado previamente e tratado com maiores taxas de sucesso. No Instituto da Dor você encontra tratamento bursite realizado por especialistas no assunto. Com diversos tipos de terapias e procedimentos, aqui você encontra suporte adequado para tratamentos específicos para cada tipo de situação, consulte-nos.</p><h2>Tratamento bursite, ATM, tendinite e muito mais</h2><p>Encontre uma enorme diversidade de tratamentos e terapias com o Instituto da Dor. Aqui você tem tratamento bursite, tratamento para ATM, tendinite, cervicalgia, condromalácia, dores musculares, articulares, e muito mais. Agende um horário para atendimento profissional com nossos especialistas e encontre tratamentos específicos para seu caso. Nossa clínica fica no Tatuapé com fácil acesso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>