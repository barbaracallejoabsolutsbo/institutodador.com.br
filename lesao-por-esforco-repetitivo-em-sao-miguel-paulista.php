<?php
$title       = "Lesão por Esforço Repetitivo em São Miguel Paulista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Entre os mais comuns estão a tendinite que é a inflamação de tendões, bursite que é a inflamação das bursas e as miosites que é a inflamação dos músculos. No início, o local afetado normalmente se encontra sempre fadigado e sem capacidade de executar grandes esforços, após algum tempo começam a surgir dores, tensões e desconfortos. Consulte-nos e encontre tratamentos para Lesão por Esforço Repetitivo em São Miguel Paulista.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Instituto da Dor ganha destaque quando o assunto é Tratamentos terapêuticos. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Lesão por Esforço Repetitivo em São Miguel Paulista até Tratamento Dores no Trapézio, Clínica de Quiropraxia, Tratamento Condromalácia, Tratamento Dor Atrás do Joelho e Clínica de Acupuntura com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>