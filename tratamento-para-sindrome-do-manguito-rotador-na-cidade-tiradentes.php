<?php
$title       = "Tratamento para Síndrome do Manguito Rotador na Cidade Tiradentes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O manguito rotador é um músculo interno que trabalha em conjunto com a articulação do ombro, e que quando inflamado pode causar dores extremas e limitações musculares. No Instituto da Dor você encontra Tratamento para Síndrome do Manguito Rotador na Cidade Tiradentes, que é causada por uma tendinite ou ruptura dos tendões que estão relacionados a essa musculatura, o tratamento alivia a dor e desconforto do paciente.</p>
<p>Trabalhando há anos no mercado voltado à Tratamentos terapêuticos, a Instituto da Dor é uma empresa de grande experiente e qualificada quando se trata de Tratamento para Fascite Plantar, Tratamento Tendinite, Clínica de Liberação Miofascial, Tratamento para Síndrome do Túnel do Carpo e Liberação Miofascial Manual e Tratamento para Síndrome do Manguito Rotador na Cidade Tiradentes. Em constante busca da satisfação de nossos clientes, atuamos no mercado com o intuito de fornecer para todos que buscam pela nossa qualidade o que há de mais moderno e eficiente no segmento.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>