<?php

    $title       = "Tratamentos";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-pag">
            <h1><?php echo $h1;?></h1>
        </div>
        <div class="container">
            <div class="conteudo-pag">
                <div class="text-right">
                    <?php echo $padrao->breadcrumb(array($title)); ?>
                </div>
                <h2>Nossos Tratamentos</h2>
                <hr>
                <div class="row text-center">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <a class="btn-arendimento" href="<?php echo $url; ?>tratamentos-patologicos">
                            <img src="<?php echo $url; ?>imagens/tratamentos-patologicos.jpg" class="img-responsive" alt="Quiropraxia" title="Quiropraxia">
                            <h3>Quiropraxia</h3>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <a class="btn-arendimento" href="<?php echo $url; ?>tratamentos-preventivos">
                            <img src="<?php echo $url; ?>imagens/tratamentos-preventivos.jpg" class="img-responsive" alt="Tratamentos Preventivos" title="Tratamentos Preventivos">
                            <h3>Tratamentos Preventivos</h3>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <a class="btn-arendimento" href="<?php echo $url; ?>acupuntura">
                            <img src="<?php echo $url; ?>imagens/acupuntura.jpg" class="img-responsive" alt="Acupuntura" title="Acupuntura">
                            <h3>Acupuntura</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>