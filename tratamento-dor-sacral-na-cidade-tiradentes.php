<?php
$title       = "Tratamento Dor Sacral na Cidade Tiradentes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Tratamento Dor Sacral na Cidade Tiradentes visa tratar a dor que ocorre na articulação sacroilíaca, local onde ocorre comunicação entre a bacia e a coluna vertebral. O problema, que é um processo inflamatório, pode gerar rigidez lombar, dor nas nádegas e que podem irradiar para as pernas, parecidas com dores no ciático. Alongamentos, sessões de fisioterapia e massagens podem ajudar e muito no processo de recuperação.</p>
<p>Se deseja comprar Tratamento Dor Sacral na Cidade Tiradentes e procura por uma empresa séria e competente, a Instituto da Dor é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Tratamento Dor na Coluna, Tratamento Dores nas Costas, Quiropraxia Manual, Lesão por Esforço Repetitivo e Tratamento Bursite. Entre em contato e saiba tudo sobre Tratamentos terapêuticos com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>