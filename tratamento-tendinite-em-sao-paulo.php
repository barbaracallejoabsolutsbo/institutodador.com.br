<?php
$title       = "Tratamento Tendinite em São Paulo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A tendinite pode aparecer em qualquer tendão do corpo, mas normalmente aparece no pulso, por conta de erros posturais ao realizar tarefas diárias comuns como mexer no computador ou escrever. Para realizar qualquer tipo de tarefa contínua é recomendável que sempre se alongue antes, durante e depois da atividade, prevenindo inflamações nos tendões. A fisioterapia também funciona como Tratamento Tendinite em São Paulo.</p>
<p>Trabalhando há anos no mercado voltado à Tratamentos terapêuticos, a Instituto da Dor é uma empresa de grande experiente e qualificada quando se trata de Tratamento para Síndrome do Túnel do Carpo, Tratamento Dor na Coluna, Quiropraxia Instrumental TIQ, Tratamento Tendinite e Formigamento nas Pernas e Tratamento Tendinite em São Paulo. Em constante busca da satisfação de nossos clientes, atuamos no mercado com o intuito de fornecer para todos que buscam pela nossa qualidade o que há de mais moderno e eficiente no segmento.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>