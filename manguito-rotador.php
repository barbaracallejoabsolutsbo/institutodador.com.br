<?php

    $title       = "Manguito Rotador";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/manguito-rotador.jpg" alt="Manguito Rotador">
                        <p>O MANGUITO ROTADOR é formado por quatro músculos: o supra-espinhal, infra-espinhal, redondo menor e subescapular e são responsáveis pela rotação e estabilização do ombro.</p>

                        <p>Quando esses músculos encurtam acabam aderindo ao osso escapular causando rompimento do tendão do musculo supra-espinhal.</p>

                        <p>Tratamento: Vamos liberar as fibras desses músculos para fazer com que a articulação volte a trabalhar livre e cesse o processo inflamatório, e o rompimento do tendão do musculo supra-espinhal.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>