<?php
$title       = "Tensão Muscular";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem diversos exercícios e movimentos que você pode realizar durante o dia a dia para aliviar a tensão muscular acumulada durante o dia. Mas às vezes ainda não é o suficiente, sendo necessária uma avaliação com intervenção de algum especialista para resolver. No Instituto da Dor você encontra um grupo multidisciplinar de profissionais totalmente aptos a lhe dar suporte para questões como essas e muito mais.</p><h2>Diversas terapias e tratamentos para tensão muscular no Instituto da Dor</h2><p>Encontre auxílio profissional para diagnosticar os problemas que possam causar sua tensão muscular e tratamentos específicos para cada caso. No Instituto da Dor você trata tendinite, cervicalgia, dores musculares, articulares, atendimento e acompanhamento pós operatório e muito mais. Sessões de acupuntura, liberação miofascial, quiropraxia, entre outras especialidades, consulte-nos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>