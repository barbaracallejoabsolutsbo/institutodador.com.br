<?php
$title       = "Tratamento para Síndrome do Túnel do Carpo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As sessões de fisioterapia podem funcionar como chave essencial para o tratamento para síndrome do túnel do carpo, sendo eficaz para alívio das tensões e dores. Existem diversos outros procedimentos que podem ser realizados no Instituto da Dor, como massagens, alongamentos, liberação miofascial, acupuntura, compressas especiais, entre outros. Agende um horário conosco e garanta seu bem estar físico e mental.</p><h2>Fisioterapia, uma opção para o tratamento para síndrome do túnel do carpo</h2><p>O tratamento para síndrome do túnel do carpo vai depender muito da gravidade da patologia do paciente. Existem diversos procedimentos não medicamentosos utilizando-se de exercícios, fisioterapia, massagens e compressores, que amenizam a dor e desconforto e podem retardar a inflamação da região. Utilizar munhequeiras, evitar sobrecargas e praticar alongamentos diariamente podem ajudar bastante no processo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>