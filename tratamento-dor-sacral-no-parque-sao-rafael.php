<?php
$title       = "Tratamento Dor Sacral no Parque São Rafael";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A fisioterapia tem papel fundamental no Tratamento Dor Sacral no Parque São Rafael e em diversas outras patologias inflamatórias que acometem o corpo. Uma das áreas responsáveis por mais taxas de sucesso de recuperação e que conta com técnicas alternativas sem o uso de medicamentos ou associando a recuperação medicamentosa com exercícios e técnicas de fortalecimento e recuperação muscular, nervosa e tecidual, acelerando o processo.</p>
<p>Como uma empresa especializada em Tratamentos terapêuticos proporcionamos sempre o melhor quando falamos de Tratamento para Dor de Cabeça, Quiropraxia Manual, Clínica de Liberação Miofascial, Dor no Pescoço e Tratamento Dor Ciático. Com potencial necessário para garantir qualidade e excelência em Tratamento Dor Sacral no Parque São Rafael com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Instituto da Dor trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>