<?php

    $title       = "Liberação Miofascial";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-preventivos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>tratamentos-patologicos">
                            <h2>Quiropraxia</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ativvo">
                        <a href="<?php echo $url; ?>tratamentos-preventivos">
                            <h2>Liberação Miofascial</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>acupuntura">
                            <h2>Acupuntura</h2>
                        </a>
                    </div>
                </div> 
                <br>
                <h3>Saiba sobre Liberação Miofascial</h3>
                <hr>
                <p>A Liberação Miofascial é uma técnica de terapia manual que consiste em aplicar pressão em alguns pontos do corpo que ajudam a relaxar e alongar os músculos, com foco no alívio de dores musculares e favorecendo a mobilidade e a qualidade dos movimentos articulares, além de reduzir as aderências dos tecidos cicatriciais.</p>

                <p>O tratamento de Liberação Miofascial do Instituto da Dor é realizado por um profissional especializado, com sessões que duram de 30 a 50 minutos.</p>

                <p>Entre as doenças tratáveis pela Liberação Miofascial estão:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Artrites, Artroses, Bursites, Epicondilite e Tendinites;</li>
                    <li><i class="fas fa-check"></i> Cervicalgia, Enxaqueca, Labirintite e Tensão muscular;</li>
                    <li><i class="fas fa-check"></i> Ciatalgia, Escoliose, Hérnia de disco, Lombalgia e Túnel do carpo;</li>
                    <li><i class="fas fa-check"></i> Fascite plantar e Esporão calcâneo;</li>
                    <li><i class="fas fa-check"></i> Fibromialgia.</li>
                </ul>
                <br>

                <p>Além da Liberação Miofascial, no Instituto da Dor você também encontrará:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Acupuntura;</li>
                    <li><i class="fas fa-check"></i> Quiropraxia;</li>
                    <li><i class="fas fa-check"></i> Laserterapia.</li>
                </ul>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>