<?php
$title       = "Tratamento para Síndrome do Cotovelo de Tenista em Ermelino Matarazzo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O cotovelo de tenista, ou epicondilite lateral, é causado pelo uso excessivo do cotovelo, muito comum no esporte do tênis. É uma lesão no tendão envolvendo os músculos extensores do antebraço, e está relacionado a altas cargas de treinamento e estresse físico por tempos prolongados na região. A fisioterapia é uma ótima alternativa de Tratamento para Síndrome do Cotovelo de Tenista em Ermelino Matarazzo.</p>
<p>Como uma empresa especializada em Tratamentos terapêuticos proporcionamos sempre o melhor quando falamos de Tratamento Artrite, Tratamento ATM, Tratamento Dor Atrás do Joelho, Tratamento para Hérnia de Disco e Tratamento Dores na Coxa. Com potencial necessário para garantir qualidade e excelência em Tratamento para Síndrome do Cotovelo de Tenista em Ermelino Matarazzo com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Instituto da Dor trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>