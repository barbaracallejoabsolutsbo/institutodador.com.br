<?php
$title       = "Tratamento Artrose em Artur Alvim";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Assim que diagnosticado, o paciente deve realizar o Tratamento Artrose em Artur Alvim a fim de retardar a destruição progressiva dos tecidos que compõem as articulações. Esse tipo de patologia não tem cura, portanto, o tratamento deve retardar o avanço do desgaste da cartilagem articular e fortalecer as estruturas relacionadas. Podendo ser tratada facilmente por profissionais especializados, no Instituto da Dor você encontra todo suporte necessário.</p>
<p>Sendo uma das principais empresas do segmento de Tratamentos terapêuticos, a Instituto da Dor possui os melhores recursos do mercado com o objetivo de disponibilizar Tratamento Condromalácia, Tratamento Dores nos Joelhos, Clínica de Fisioterapia, Tratamento para Fascite Plantar e Tratamento para Torcicolo com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Tratamento Artrose em Artur Alvim com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>