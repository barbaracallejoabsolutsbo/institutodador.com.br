<?php
$title       = "Ciático Inflamado na Aclimação";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conheça o Instituto da Dor, uma clínica com atendimentos diversificados, entre eles fisioterapia, a técnica terapêutica Shian, acupuntura, entre outros procedimentos para solução de dores, tensões e incômodos musculares e articulares. Com essas técnicas é possível resolver problemas de dores de Ciático Inflamado na Aclimação, assim como diversos outros problemas crônicos patológicos ou até mesmo para prevenção.</p>
<p>Além de sermos uma empresa especializada em Ciático Inflamado na Aclimação disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Tratamento Artrose, Tratamento Ciatalgia, Formigamento nas Pernas, Tratamento para Esporão Calcâneo e Tratamento Dores na Coxa. Com a ampla experiência que a equipe Instituto da Dor possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Tratamentos terapêuticos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>