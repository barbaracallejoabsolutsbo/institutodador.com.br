<?php

    $title       = "Quiropraxia";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ativvo">
                        <a href="<?php echo $url; ?>tratamentos-patologicos">
                            <h2>Quiropraxia</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>tratamentos-preventivos">
                            <h2>Liberação Miofascial</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>acupuntura">
                            <h2>Acupuntura</h2>
                        </a>
                    </div>
                </div> 
                <br>
                <p>A Quiropraxia consiste em uma técnica com foco na coluna vertebral, que sustenta todo o corpo. Esta terapia manual pode ajudar a tratar problemas relacionados a articulações, músculos, ossos, tendões e ligamentos - ou seja, tudo o que diz respeito aos distúrbios biomecânicos do corpo humano. </p>

                <p>O tratamento de Quiropraxia do Instituto da Dor é realizado por um profissional especializado, com sessões que duram de 30 a 50 minutos.</p>

                <p>Entre as doenças tratáveis pela Quiropraxia estão:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Dores no pescoço, lombar e cabeça;</li>
                    <li><i class="fas fa-check"></i> Tensões musculares;</li>
                    <li><i class="fas fa-check"></i> Lesões esportivas ou de acidentes variados;</li>
                    <li><i class="fas fa-check"></i> Desvios na postura.</li>
                </ul>
                <br>

                <p>Além da Quiropraxia, no Instituto da Dor você também encontrará:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Acupuntura;</li>
                    <li><i class="fas fa-check"></i> Liberação Miofascial;</li>
                    <li><i class="fas fa-check"></i> Laserterapia.</li>
                </ul>
                <div class="cont-tratamentos">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Artrite</h4>
                            <p>Artrite é inflamação das articulações, em sentido amplo, conjunto de sintomas e sinais resultantes de…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>artrite">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Artrose</h4>
                            <p>É a destruição progressiva dos tecidos que compõem as articulações. As áreas do corpo mais…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>artrose">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Bursite</h4>
                            <p>Bursite significa um processo inflamatório da bolsa serosa chamada bursa, que se encontra em diversas…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>bursite">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Capsulite articular</h4>
                            <p>Ocorre na cápsula articular da glenóidea com perda de movimentos ativos e passivos do ombro.…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>capsulite-articular">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Cervicalgia / Cervicobraquialgia</h4>
                            <p>É uma inflamação na região cervical, que é causada devido principalmente a problemas posturais, onde…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>cervicalgia-cervicobraquialgia">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Ciatalgia</h4>
                            <p>Os casos mais comuns de Ciatalgia ocorre na altura do músculo piriforme, pois é onde…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>ciatalgia">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Epicondilite</h4>
                            <p>A epicondilite, também chamada de "cotovelo do tenista" (tennis elbow), é uma inflamação ou irritação…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>epicondilite">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Escoliose</h4>
                            <p>Curvatura lateral da coluna, mais correto dizer, curvatura látero-lateral rotacional. Essa deformidade pode levar a…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>escoliose">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Esporão Calcâneo</h4>
                            <p>Esporão do calcâneo é uma formação óssea reativa em forma de esporão localizada na face…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>esporao-calcaneo">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Fibromialgia</h4>
                            <p>O termo fibromialgia refere-se a uma condição dolorosa generalizada e crônica. É considerada uma síndrome…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>fibromialgia">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Hérnia de Disco</h4>
                            <p>A hérnia de disco é um problema causado por uma compressão no disco intervertebral devido…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>hernia-de-disco">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Hipercifose</h4>
                            <p>Traduzindo do Grego cifose (GR. Kyphosis, giba), significa gibosidade, ou seja, um aumento pronunciado da…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>hipercifose">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Lombalgia</h4>
                            <p>Essas dores na lombar geralmente são causadas por problemas na musculatura do glúteo e quadríceps.…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>lombalgia">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Manguito Rotador</h4>
                            <p>O MANGUITO ROTADOR é formado por quatro músculos: o supra-espinhal, infra-espinhal, redondo menor e subescapular…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>manguito-rotador">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Tendinite</h4>
                            <p>É uma neuropatia, causada devido a uma compressão do nervo mediano no canal do carpo,…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>tendinite">Leia mais</a>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <h4>Túnel do carpo</h4>
                            <p>Tendinite é uma síndrome de excesso de uso em resposta a inflamação local devido a…</p>
                            <a class="btn-leiamais" href="<?php echo $url; ?>tunel-do-carpo">Leia mais</a>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>