<?php
$title       = "Laserterapia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Laserterapia é um tipo de terapia feita com laser de baixa potência e que atende a vários ramos clínicos. Muito útil para tratar lesões musculares, estimular recuperação e cicatrização tecidual. Popularmente utilizado também para tratar processos inflamatórios e dores musculares, o laser de baixa potência muda no estado Redox celular em suas relações com o oxigênio.</p><h2>Para quais casos a laserterapia pode ser utilizada?</h2><p>Comumente utilizado na fisioterapia a fim de estimular a recuperação de tecidos musculares, combater inflamações e dores, a laserterapia trata dores crônicas, artrite reumatóide, osteoartrite, dor nas articulações, dores miofasciais, epicondilite lateral, alterações envolvendo nervos periféricos, e até mesmo para tratamento de nervo ciático, com casos de ótimos resultados. Agende já uma sessão conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>