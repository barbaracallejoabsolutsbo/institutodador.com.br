<?php
$title       = "Tratamento Dor nas Mãos na Sapopemba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor é referência em tratamentos relacionados à fisioterapia. Aqui, o Tratamento Dor nas Mãos na Sapopemba é feito com massagens, alongamentos, liberação miofascial, quiropraxia, acupuntura, fisioterapia, entre outros procedimentos. Isso porque acreditamos no potencial físico de regeneração e na fisioterapia. Se o seu quadro patológico permitir, evitamos o uso de qualquer tipo de medicamento.</p>
<p>Tendo como especialidade Tratamento para Dor, Tratamento para Dor de Cabeça, Tratamento Epicondilite, Tratamento Dores na Coxa e Clínica de Fisioterapia, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Tratamentos terapêuticos. Por isso, quando falamos de Tratamento Dor nas Mãos na Sapopemba, buscar pelos membros da empresa Instituto da Dor é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>