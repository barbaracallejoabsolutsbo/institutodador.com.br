<?php
$title       = "Dor no Pescoço";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem diversas formas de aliviar uma dor no pescoço, mas em alguns casos a dor pode ser crônica ou muito forte, causando grandes desconfortos. Nesses casos é muito importante procurar suporte profissional adequado. O Instituto da Dor é uma clínica especializada em diagnóstico, tratamentos e terapias para dores musculares, articulares, dores no pescoço, na coluna, joelhos, entre outros</p><h2>Especialista em dor no pescoço, no Instituto da Dor você encontra!</h2><p>Com equipe multidisciplinar, no Instituto da Dor você tem todo suporte para solucionar dor no pescoço, joelho, coluna, entre outros. Agende sua consulta para diagnóstico e tenha profissionais para atendimento de diversas terapias para prevenir ou tratar patologias articulares, musculares, entre outros. Encontre atendimento e solução para suas dores no Instituto da Dor.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>