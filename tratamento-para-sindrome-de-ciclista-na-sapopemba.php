<?php
$title       = "Tratamento para Síndrome de Ciclista na Sapopemba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A síndrome do ciclista é um tipo de síndrome gerada por pressão na região do períneo, local que fica apoiado sobre o banco da bicicleta, causando compressão nervosa em aproximadamente 60 a 70% dos ciclistas habituais. Essa compressão acaba irritando e inflamando a região causando alteração na movimentação, sensibilidade e problemas vasculares na região. Consulte nosso Tratamento para Síndrome de Ciclista na Sapopemba.</p>
<p>Na busca por uma empresa qualificada em Tratamentos terapêuticos, a Instituto da Dor será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Tratamento Artrose, Quiropraxia Instrumental TIQ, Tratamento para Esporão Calcâneo, Tratamento Ciatalgia e Quiropraxia Manual com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Tratamento para Síndrome de Ciclista na Sapopemba.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>