<?php

    $title       = "Esporão Calcâneo";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/esporao-calcaneo.jpg" alt="Esporão Calcâneo">
                        <p>Esporão do calcâneo é uma formação óssea reativa em forma de esporão localizada na face plantar do calcâneo, os sintomas são: dor na região plantar sob o calcâneo, que é pior no início da manhã e durante atividades prolongadas em posição de apoio podálico.</p>

                        <p>Os músculos que causam o esporão calcâneo são os músculos da panturrilha e o flexor curto dos dedos.</p>

                        <p>Quando há uma contratura da musculatura da panturrilha, o tendão calcâneo passa a trabalhar forçado e conseqüentemente o flexor curto dos dedos fica sobrecarregado e também é prejudicado ocasionando a fascite plantar.</p>

                        <p>Como Tratar: fazemos a liberação da contratura muscular da panturrilha e do flexor curto dos dedos. Em seguida, alongamos a musculatura que volta a sua normalidade e a fascite plantar cessa.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>