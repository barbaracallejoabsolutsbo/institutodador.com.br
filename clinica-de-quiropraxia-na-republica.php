<?php
$title       = "Clínica de Quiropraxia na República";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Através de manobras que corrige desalinhamentos, erros posturais, tensões musculares, entre outros, a quiropraxia busca tratar nervos, articulações e ossos através de técnicas realizadas com as mãos que aliviam a tensão, ativam a circulação e a reparação do local aplicado, ainda melhorando a flexibilidade. Agende um horário em nossa Clínica de Quiropraxia na República e faça uma sessão conosco.</p>
<p>Pensando em proporcionar a todos os clientes o melhor em métodos quando se trata de Tratamentos terapêuticos? A empresa Instituto da Dor vem ganhando destaque em referência no assunto de Clínica de Quiropraxia na República. Por isso, solicite um orçamento para Liberação Miofascial Manual, Tratamento Bursite, Tratamento Cervicobraquialgia, Tratamento para Torcicolo e Tratamento Dores no Trapézio assim você contará com a qualidade que somente a nossa empresa oferece para todos os seus clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>