<?php
$title       = "Tratamento para Dores de Cabeça no Bom Retiro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está em busca de Tratamento para Dores de Cabeça no Bom Retiro, o Instituto da Dor possui profissionais altamente qualificados para tratar dores corporais em geral sem a utilização de medicamentos ou intervenções severas. São terapias alternativas como acupuntura, massagens e outros procedimentos que fazem parte desse tipo de tratamento, com alta taxa de sucesso e satisfação em relação aos resultados e bem estar pessoal.</p>
<p>Entre em contato com a Instituto da Dor se você busca por Tratamento para Dores de Cabeça no Bom Retiro. Somos uma empresa especializada com foco em Clínica de Fisioterapia, Tensão Muscular, Tratamento Dor Atrás do Joelho, Clínica de Liberação Miofascial e Lesão por Esforço Repetitivo onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de Tratamentos terapêuticos. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>