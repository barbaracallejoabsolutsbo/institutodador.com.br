<?php
$title       = "Clínica de Liberação Miofascial na Consolação";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Com massagens manuais, rolos e ferramentas especiais, é feita uma massagem forte a fim de estimular a elasticidade e flexibilidade do tecido que envolve a musculatura, facilitando a liberação da tensão e melhorando a contração muscular. Muito utilizado por atletas de alto rendimento, inclusive no fisiculturismo, reduz a sensibilidade a dor e previne lesões. Agende sua consulta em nossa Clínica de Liberação Miofascial na Consolação.</p>
<p>Tendo como especialidade Laserterapia, Clínica de Fisioterapia, Home Care, Dry Needling e Tratamento para Hérnia de Disco, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Tratamentos terapêuticos. Por isso, quando falamos de Clínica de Liberação Miofascial na Consolação, buscar pelos membros da empresa Instituto da Dor é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>