<?php
$title       = "Tratamento para Síndrome do Piriforme na Vila Esperança";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As sessões de fisioterapia podem funcionar como chave essencial para o Tratamento para Síndrome do Piriforme na Vila Esperança, sendo eficaz para alívio das tensões e dores. Existem diversos outros procedimentos que podem ser realizados no Instituto da Dor, como massagens, alongamentos, liberação miofascial, acupuntura, compressas especiais, entre outros. Agende um horário conosco e garanta seu bem estar físico e mental.</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de Tratamentos terapêuticos, a Instituto da Dor é uma empresa experiente quando se trata do ramo de Tratamento para Dores de Cabeça, Tratamento para Esporão Calcâneo, Tratamento Artrite, Tratamento Tendinite e Liberação Miofascial Manual. Por isso, se você busca o melhor com um custo acessível e vantajoso em Tratamento para Síndrome do Piriforme na Vila Esperança aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>