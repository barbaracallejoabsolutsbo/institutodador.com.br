<?php
$title       = "Tratamento Dores nas Costas";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A melhor opção de tratamento dores nas costas com certeza é utilizar de fisioterapia e exercícios físicos a fim de fortalecer, ganhar flexibilidade e resistência muscular. A musculatura da coxa é solicitada praticamente o dia todo em grande parte da população mundial, e ter essa estrutura bem cuidada, saudável, sem o uso de medicamentos, são a melhor forma de prevenir que dores surjam novamente.</p><h2>Tratamento dores nas costas de forma alternativa, sem medicamentos</h2><p>O Instituto da Dor é reconhecido por solucionar problemas de dor física sem a utilização de meios medicamentosos, apenas com fisioterapia, terapias alternativas e exercícios. Apenas em casos de patologias mais severas ou degenerativas, os problemas inflamatórios musculares simples podem ser resolvidos com fisioterapia e fortalecimento muscular. Faça tratamento dores nas costas com o Instituto da Dor.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>