<?php

    $title       = "Fisioterapia";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "fisioterapia"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-pag">
            <h1><?php echo $h1;?></h1>
        </div>
        <div class="container">
            <div class="conteudo-pag">
                <div class="text-right">
                    <?php echo $padrao->breadcrumb(array($title)); ?>
                </div>
                <h2>Saiba sobre Fisioterapia</h2>
                <hr>
                <img class="img-right" src="<?php echo $url; ?>imagens/fisioterapia.jpg" alt="Fisioterapia">
                <p>Fisioterapia é a ciência que estuda, diagnostica, previne e recupera pacientes com distúrbios cinéticos funcionais intercorrentes em órgãos e sistemas do corpo humano. Trabalha com doenças geradas por alterações genéticas, traumas ou enfermidades adquiridas, como vícios posturais, tendinites, bursites, hérnia de disco, dores musculares e articulares, entre outras.</p>
                <p>É uma ciência tão antiga quanto o homem. Surgiu com as primeiras tentativas dos ancestrais de diminuir uma dor esfregando o local dolorido e evoluiu ao longo do tempo com a sofisticação, principalmente, das técnicas de exercícios terapêuticos.</p>
                <p>O objetivo desta área é preservar, manter, desenvolver ou restaurar (reabilitação) a integridade de órgãos, sistemas ou funções. Utiliza-se de conhecimento e recursos próprios como parte do processo terapêutico nas condições psico-físico-social para promover o alívio das dores e melhoria da qualidade de vida.</p>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>