<?php
$title       = "Tratamento para Dor de Cabeça no Parque São Rafael";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor trabalha com métodos e técnicas alternativas para Tratamento para Dor de Cabeça no Parque São Rafael. Muitas pessoas hoje em dia sofrem de dores de cabeça por conta do estresse diário, cansaço, ansiedade e má alimentação. Existem diversos procedimentos que podem ser encontrados no Instituto da Dor a fim de amenizar esse incômodo, como acupuntura, massagens e procedimentos de compressas para redução da dor.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Instituto da Dor ganha destaque quando o assunto é Tratamentos terapêuticos. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Tratamento para Dor de Cabeça no Parque São Rafael até Laserterapia, Tratamento para Escoliose, Tratamento Ciatalgia, Tratamento Bursite e Tratamento Dores nos Joelhos com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>