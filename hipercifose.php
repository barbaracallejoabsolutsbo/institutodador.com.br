<?php

    $title       = "Hipercifose";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
<!--                         <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/hipercifose.jpg">
 -->                        
                        <p>Traduzindo do Grego cifose (GR. Kyphosis, giba), significa gibosidade, ou seja, um aumento pronunciado da concavidade da curva torácica, corcunda”. Esta alteração postural ou funcional é também denominada como dorso curvo ou postura senil, sendo esta pouco mais rígida uma vez que possa já ter acometido outras estruturas como ossos, ligamentos, disco intervertebral com ou sem o deslocamento do núcleo pulposo.</p>

                        <p>A Hipercifose deixa os músculos da região torácica fortes e curtos, causando a envergadura das costas.</p>

                        <p>Como Tratar: Nossa técnica trata diretamente os causadores que são os músculos curtos e fortes. Liberamos as fibras musculares que estão encurtadas ou até mesmo atrofiadas, alinhamos a estrutura óssea, fazemos a correção postural, fortalecemos os músculos que estão alongados e fracos.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>