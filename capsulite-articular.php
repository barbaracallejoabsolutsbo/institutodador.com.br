<?php

    $title       = "Capsulite articular";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/capsulite-articular.jpg" alt="Capsulite articular">
                        <p>Ocorre na cápsula articular da glenóidea com perda de movimentos ativos e passivos do ombro.</p>
                        <p>Causa quando os músculos elevadores da escapula, ROMBOIDES e MANGUITO ROTADOR aderem ao osso escapular,e faz com que a escapula não gire e perca todo o movimento articular.</p>
                        <p>Como tratar: Vamos liberar todas as fibras musculares que estão aderidas A estrutura óssea, para que a articulação volte ter os movimentos normalizados e livres, cessando a capsulite.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>