<?php
$title       = "Tratamento Dor Ciático na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A fisioterapia é uma das melhores alternativas para Tratamento Dor Ciático na Vila Carrão. Em algumas sessões já é possível observar melhorias significativas em relação ao fortalecimento e alívio da dor. No Instituto da Dor você ainda tem outras ferramentas como laserterapia, tratamentos térmicos, acupuntura, osteopatia, entre outros métodos que aceleram o tratamento e podem auxiliar outras regiões do corpo.</p>
<p>Se deseja comprar Tratamento Dor Ciático na Vila Carrão e procura por uma empresa séria e competente, a Instituto da Dor é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Tratamento para Síndrome do Cotovelo de Tenista, Tratamento para Fibromialgia, Tratamento Tendinite, Tratamento para Síndrome do Túnel do Carpo e Tratamento Dor no Ombro. Entre em contato e saiba tudo sobre Tratamentos terapêuticos com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>