<?php

    $title       = "Fibromialgia";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/fibromialgia.jpg" alt="Fibromialgia">
                        <p>O termo fibromialgia refere-se a uma condição dolorosa generalizada e crônica. É considerada uma síndrome porque engloba uma série de manifestações clínicas como dor, fadiga, indisposição, distúrbios do sono.</p>

                        <p>A fibromialgia primária começa por problemas musculares não tratados, já a secundária passa a ser psicossomática. Pois, devido as dores referentes à inflamação dos grupos musculares a pessoa passa a não dormir direito, apresenta muita ansiedade, depressão , passando a somatizar tudo.</p>

                        <p>Como Tratar: Nossa técnica vai tratar os causadores que são as fibras musculares. Liberamos as fibras musculares encurtadas ou contraturadas, alongamos os músculos acometidos, recomendamos exercícios físicos e caminhadas e gradativamente a pessoa vai percebendo a melhora do processo inflamatório e a diminuição das dores.</p>
                        <h2>OBS: TRATAMENTO NÃO INVASIVO E NÃO MEDICAMENTOSO</h2>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>