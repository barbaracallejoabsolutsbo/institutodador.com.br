<?php
$title       = "Tratamento Dores no Trapézio na Consolação";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As dores na região do trapézio normalmente estão relacionadas a sobrecarga e excesso de uso da musculatura, desequilíbrios nervosos e viscerais causados por conta de compressão dos nervos espinhais. Com sessões de fisioterapia e terapias como acupuntura, liberação miofascial, entre outros, é possível fazer Tratamento Dores no Trapézio na Consolação sem uso de medicamentos, solucionando desconfortos.</p>
<p>A Instituto da Dor é uma empresa que se destaca no setor de Tratamentos terapêuticos, pois oferece não somente Tratamento Dor Sacral, Ciático Inflamado, Tensão Muscular, Liberação Miofascial Manual e Tratamento Dores na Coxa, mas também, Tratamento Dores no Trapézio na Consolação com o intuito de atender às mais exigentes solicitações. Entre em contato e faça uma cotação com um de nossos competentes profissionais e assim comprove que somos a empresa que trabalha com o foco na qualidade e satisfação de nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>