<?php

    $title       = "Artrose";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/artrose.jpg" alt="Artrose">
                        <p>É a destruição progressiva dos tecidos que compõem as articulações. As áreas do corpo mais comprometidas são os joelhos. A artrose não tem cura, mas através do nosso método de tratamento conseguimos fazer com que o desgaste da cartilagem cesse, diminuindo a dor e a rigidez articular.</p>
                        <p>O desgaste da cartilagem (artrose) começa geralmente devido a problemas musculares como uma contratura no quadríceps fazendo com que essa musculatura calce a patela dificultando a flexão da articulação.</p>
                        <p>Como Tratar: A partir do momento em que liberamos essa musculatura, a patela fica livre fazendo com que a articulação tenha uma flexão normalizada.</p>
                        <h2>OBS: QUANDO JÁ HOUVE O DESGASTE TOTAL DA CARTILAGEM O TRATAMENTO É CIRÚRGICO</h2>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>