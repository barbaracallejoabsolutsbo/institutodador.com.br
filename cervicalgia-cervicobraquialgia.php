<?php

    $title       = "Cervicalgia / Cervicobraquialgia";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/cervicalgia-cervicobraquialgia.jpg" alt="Cervicalgia / Cervicobraquialgia">
                        <p>É uma inflamação na região cervical, que é causada devido principalmente a problemas posturais, onde a musculatura da região escapular e cervical acometem causando o encurtamento das fibras musculares, levando a compressão dos discos intervertebrais e consequentemente o processo inflamatório no local.</p>

                        <p>Quando aumenta compressão e pinça os nervos, o quadro evolui para uma cervicobraquialgia porque as dores começam a irradiar para os membros superiores causando parestesia e sensação de formigamento e dormência.</p>

                        <p>Como tratar: Vamos liberar as fibras musculares que estão acometidas para que as articulações voltem a sua normalidade cessando a compressão nos nervos e consequentemente o processo inflamatório, parestesia, formigamento e dormência.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>