<?php

    $title       = "Artrite";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/artrite.jpg" alt="Artrite">
                        <p>Artrite é inflamação das articulações, em sentido amplo, conjunto de sintomas e sinais resultantes de lesões articulares produzidas por diversos motivos e causas. Popularmente a artrite é sinônimo de reumatismo, designação genérica para a dor, rigidez ou deformidade de articulações e estruturas vizinhas aos músculos, muitas vezes esse processo começa com uma desordem muscular que não está dando a devida proteção na articulação.</p>
                        <p>Como Tratar: Quando essas inflamações forem causadas por problemas musculares basta liberarmos as fibras musculares correspondentes, para que a articulação trabalhe livre e cesse a inflamação.</p>
                        <h2>TRATAMENTO NÃO INVASIVO E NÃO MEDICAMENTOSO</h2>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>