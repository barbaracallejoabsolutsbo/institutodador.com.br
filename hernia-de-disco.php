<?php

    $title       = "Hérnia de Disco";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/hernia-de-disco.jpg" alt="Hérnia de Disco">
                        <p>A hérnia de disco é um problema causado por uma compressão no disco intervertebral devido a uma desordem muscular toracolombar ou até mesmo membros inferiores.</p>

                        <p>Como tratar: nossa técnica consiste em liberar as fibras dos músculos e as possíveis contraturas e ou até mesmo atrofia.</p>

                        <p>Quando liberamos e alongamos os músculos a compressão nos discos intervertebrais diminui e a herniação vai desaparecendo.</p>

                        <p>Obs: essa nossa técnica é revolucionária , o tratamento não é invasivo e não medicamentoso.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>