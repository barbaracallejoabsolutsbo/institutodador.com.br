<?php
$title       = "Clínica de Fisioterapia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor é uma clínica de fisioterapia, que conta também com acupuntura e técnicas especiais como o Shian. Nossa clínica realiza procedimentos e tratamentos de cunho preventivo e também para casos patológicos. Agende uma consulta conosco para diagnósticos e avaliação profissional, opte por tratamentos específicos para seu caso ou encontre procedimentos preventivos para bem estar e qualidade de vida.</p><h2>Conheças os tratamentos disponíveis em nossa clínica de fisioterapia</h2><p>Aqui você encontra o que há de mais moderno para tratamentos terapêuticos e acupuntura, sem necessidade de quaisquer intervenções invasivas, proporcionando melhora do bem estar físico e mental. Encontre solução para tensões musculares, articulares, enrijecimento ou encurtamento de tendões realizando procedimentos em nossa clínica de fisioterapia localizada no Tatuapé. Contate-nos para agendamentos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>