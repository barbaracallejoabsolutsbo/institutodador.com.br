<?php
$title       = "Tratamento Artrose na Vila Curuçá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Assim que diagnosticado, o paciente deve realizar o Tratamento Artrose na Vila Curuçá a fim de retardar a destruição progressiva dos tecidos que compõem as articulações. Esse tipo de patologia não tem cura, portanto, o tratamento deve retardar o avanço do desgaste da cartilagem articular e fortalecer as estruturas relacionadas. Podendo ser tratada facilmente por profissionais especializados, no Instituto da Dor você encontra todo suporte necessário.</p>
<p>Você procura por Tratamento Artrose na Vila Curuçá? Contar com empresas especializadas no segmento de Tratamentos terapêuticos é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Instituto da Dor é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Tratamento Epicondilite, Tratamento para Esporão Calcâneo, Tratamento para Dores de Cabeça, Tratamento Dor na Coluna e Tratamento Dores na Lombar e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>