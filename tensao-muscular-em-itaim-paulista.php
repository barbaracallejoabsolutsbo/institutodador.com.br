<?php
$title       = "Tensão Muscular em Itaim Paulista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem diversos exercícios e movimentos que você pode realizar durante o dia a dia para aliviar a Tensão Muscular em Itaim Paulista acumulada durante o dia. Mas às vezes ainda não é o suficiente, sendo necessária uma avaliação com intervenção de algum especialista para resolver. No Instituto da Dor você encontra um grupo multidisciplinar de profissionais totalmente aptos a lhe dar suporte para questões como essas e muito mais.</p>
<p>Especializada em Tensão Muscular em Itaim Paulista, a Instituto da Dor ganha destaque no mercado de Tratamentos terapêuticos em que atua com experiência e amplo conhecimento em diferentes vertentes como Clínica de Quiropraxia, Tratamento Bursite, Tratamento para Hérnia de Disco, Tratamento Dores nos Joelhos e Tratamento Dores nas Costas. Unindo profissionais qualificados para um atendimento de alto padrão e dedicação, nossa empresa, hoje, é destaque entre as melhores do ramo. Conte com os nossos profissionais para garantir o melhor em que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>