<?php
$title       = "Tratamento Dores nos Joelhos na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre as melhores ofertas e condições para Tratamento Dores nos Joelhos na Vila Carrão com o Instituto da Dor. Estamos na Zona Leste de São Paulo, no Tatuapé. Temos serviços especializados de fisioterapia e terapias alternativas para tratamento e prevenção de diversas patologias musculares, articulares e físicas. Agende um horário conosco para avaliação profissional e encontre terapias ideais para seu caso.</p>
<p>Com a Instituto da Dor proporcionando o que se tem de melhor e mais moderno no segmento de Tratamentos terapêuticos consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Tratamento para Síndrome de Ciclista, Tratamento Artrose, Tratamento Dores no Trapézio, Tratamento Dores nos Joelhos e Tratamento para Síndrome do Manguito Rotador nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Tratamento Dores nos Joelhos na Vila Carrão.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>