<?php
$title       = "Tratamento Dor Sacral em Engenheiro Goulart";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A fisioterapia tem papel fundamental no Tratamento Dor Sacral em Engenheiro Goulart e em diversas outras patologias inflamatórias que acometem o corpo. Uma das áreas responsáveis por mais taxas de sucesso de recuperação e que conta com técnicas alternativas sem o uso de medicamentos ou associando a recuperação medicamentosa com exercícios e técnicas de fortalecimento e recuperação muscular, nervosa e tecidual, acelerando o processo.</p>
<p>Na busca por uma empresa referência, quando o assunto é Tratamentos terapêuticos, a Instituto da Dor será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Tratamento para Dor de Cabeça, Clínica de Acupuntura, Tratamento ATM, Lesão por Esforço Repetitivo e Tratamento Dores nos Joelhos, oferece Tratamento Dor Sacral em Engenheiro Goulart com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>