<?php
$title       = "Home Care no Glicério";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conheça o Instituto da Dor, uma clínica que presta serviços especializados de fisioterapia, quiropraxia, tratamentos de lesões por esforço, problemas musculares, articulares, entre outros. Aqui você tem serviço de Home Care no Glicério acessível para todas essas especialidades acima citadas e ainda mais. Consulte nosso catálogo de serviços e entre em contato para mais informações.</p>
<p>Trabalhando para garantir Home Care no Glicério de maior qualidade no segmento de Tratamentos terapêuticos, contar com a Instituto da Dor que, proporciona, também, Tratamento para Síndrome do Piriforme, Tratamento para Escoliose, Tratamento para Síndrome do Cotovelo de Tenista, Clínica de Liberação Miofascial e Tratamento Ciatalgia com a mesma excelência e dedicação se torna a melhor decisão para você. Se destacando de forma positiva das demais empresas, nós contamos com profissionais amplamente capacitados para um atendimento de sucesso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>