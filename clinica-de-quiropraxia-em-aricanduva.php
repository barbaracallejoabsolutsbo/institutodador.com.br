<?php
$title       = "Clínica de Quiropraxia em Aricanduva";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Através de manobras que corrige desalinhamentos, erros posturais, tensões musculares, entre outros, a quiropraxia busca tratar nervos, articulações e ossos através de técnicas realizadas com as mãos que aliviam a tensão, ativam a circulação e a reparação do local aplicado, ainda melhorando a flexibilidade. Agende um horário em nossa Clínica de Quiropraxia em Aricanduva e faça uma sessão conosco.</p>
<p>Buscando por uma empresa de credibilidade no segmento de Tratamentos terapêuticos, para que, você que busca por Clínica de Quiropraxia em Aricanduva, tenha a garantia de qualidade e idoneidade, contar com a Instituto da Dor é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Tratamento Dor no Ombro, Dor no Pescoço, Tratamento para Fibromialgia, Tratamento Dor Ciático e Tratamento Dores na Coxa para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>