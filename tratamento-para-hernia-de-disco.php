<?php
$title       = "Tratamento para Hérnia de Disco";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A hérnia de disco é causada pelo desgaste, deslocamento ou envelhecimento do disco intervertebral. O disco intervertebral é um sistema que absorve o impacto dos movimentos do corpo que ocorrem entre as vértebras. Causando fragilidade, dores e desconfortos, no Instituto da Dor você encontra profissionais especializados em tratamento para hérnia de disco, agende uma consulta em nossa clínica.</p><h2>Tratamento para hérnia de disco no Tatuapé e região</h2><p>Com as melhores ofertas, preços e condições, o Instituto da Dor, localizado no Tatuapé, Zona Leste de São Paulo, oferece tratamento para hérnia de disco com terapias alternativas e fisioterapia. Entre sessões de laserterapia, quiropraxia, alongamento, entre outros, cuide da saúde da sua coluna e das suas articulações na maior clínica de fisioterapia de São Paulo e região.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>