<?php
$title       = "Liberação Miofascial Manual";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor possui atendimento especializado em liberação miofascial manual. Esse tipo de procedimento visa amenizar dores musculares causadas por tensões e nós musculares. É realizado manualmente ou com utilização de ferramentas e instrumentos especiais, movimentos de pressão sob os locais de dor, com objetivo de prevenir e tratar patologias musculoesqueléticas.</p><h2>Liberação miofascial manual ou com instrumentos, qual a diferença?</h2><p>Ambos processos têm o mesmo objetivo, a partir de pontos específicos, gerar pressão nos locais onde existem desconfortos e dores musculares. Na liberação miofascial manual, esses pontos de pressão são acessados com as mãos, com maior capacidade de controle por conta do tato. Em casos mais específicos são utilizados rolos, bolas e ganchos para acessar locais mais profundos e soltar a musculatura de forma completa.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>