<?php
$title       = "Tratamento para Escoliose na Ponte Rasa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Muito indicado para Tratamento para Escoliose na Ponte Rasa, enxaquecas de origem energética ou inflamatória, esse tipo de método não é invasivo e possui efeito analgésico e anti-inflamatório sem a utilização de remédios, sendo uma alternativa muito procurada de tratamento para dores de cabeça sem utilização de remédios prescritos. Agende uma consulta no Instituto da Dor e faça uma avaliação para diagnóstico profissional.</p>
<p>A empresa Instituto da Dor possui uma ampla experiência no segmento de Tratamentos terapêuticos, de onde vem atuando em Tratamento para Escoliose na Ponte Rasa com dedicação e proporcionando os melhores resultados, garantindo sempre a excelência. Assim, vem se destacando de forma positiva das demais empresas do mercado que vem atuando com total empenho em Laserterapia, Tratamento Cervicalgia, Tratamento Dor no Ombro, Tratamento para Síndrome do Manguito Rotador e Tratamento Dor Ciático. Entre em contato conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>