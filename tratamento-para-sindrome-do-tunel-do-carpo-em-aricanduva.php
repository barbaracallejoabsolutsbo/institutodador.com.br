<?php
$title       = "Tratamento para Síndrome do Túnel do Carpo em Aricanduva";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Tratamento para Síndrome do Túnel do Carpo em Aricanduva vai depender muito da gravidade da patologia do paciente. Existem diversos procedimentos não medicamentosos utilizando-se de exercícios, fisioterapia, massagens e compressores, que amenizam a dor e desconforto e podem retardar a inflamação da região. Utilizar munhequeiras, evitar sobrecargas e praticar alongamentos diariamente podem ajudar bastante no processo.</p>
<p>Como uma empresa especializada em Tratamentos terapêuticos proporcionamos sempre o melhor quando falamos de Tratamento para Síndrome do Piriforme, Lesão por Esforço Repetitivo, Clínica de Liberação Miofascial, Tratamento Dor Atrás do Joelho e Quiropraxia Instrumental TIQ. Com potencial necessário para garantir qualidade e excelência em Tratamento para Síndrome do Túnel do Carpo em Aricanduva com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Instituto da Dor trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>