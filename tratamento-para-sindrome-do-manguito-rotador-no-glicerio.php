<?php
$title       = "Tratamento para Síndrome do Manguito Rotador no Glicério";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O manguito rotador é um músculo interno que trabalha em conjunto com a articulação do ombro, e que quando inflamado pode causar dores extremas e limitações musculares. No Instituto da Dor você encontra Tratamento para Síndrome do Manguito Rotador no Glicério, que é causada por uma tendinite ou ruptura dos tendões que estão relacionados a essa musculatura, o tratamento alivia a dor e desconforto do paciente.</p>
<p>Se está procurando por Tratamento para Síndrome do Manguito Rotador no Glicério e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Instituto da Dor é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Tratamentos terapêuticos conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Dor no Pescoço, Ciático Inflamado, Tratamento para Dores de Cabeça, Tratamento para Dor e Formigamento nas Pernas.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>