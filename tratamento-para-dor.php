<?php
$title       = "Tratamento para Dor";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O melhor tipo de tratamento para dor é a fisioterapia. O profissional que atende esse tipo de especialidade é responsável por estudar, diagnosticar e tratar dores corporais de diversos tipos de pacientes. O Instituto da Dor está no mercado há mais de 35 anos atuando na área de fisioterapia com diversas terapias alternativas disponíveis para tratamento de patologias relacionadas à musculatura, tendões, articulações e ossos.</p><h2>Tratamento para dor física, muscular, articular, entre outros</h2><p>Conheça o Instituto da Dor, somos uma clínica especializada em fisioterapia e terapias alternativas para patologias físicas relacionadas a musculatura, articulações, tendões e ossos. Diversos tipos de tratamento para dor visando a saúde e bem estar pessoal. Trabalhamos com profissionais altamente qualificados e especializados visando sua saúde com compromisso e excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>