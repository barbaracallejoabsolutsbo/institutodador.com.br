<?php

    $title       = "Bursite";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/bursite.jpg" alt="Bursite">
                        <p>Bursite significa um processo inflamatório da bolsa serosa chamada bursa, que se encontra em diversas articulações do corpo, inclusive no ombro, quadril.</p>

                        <p>A bursite mais comum é a do ombro e é onde as pessoas sentem muitas dores na articulação. Este tipo de lesão é causada por esforço repetitivo (LER) ou distúrbios osteoligamentares relacionados ao trabalho (DORT).</p>

                        <p>Geralmente ocorre um encurtamento dos músculos do braço fazendo com que os tendões passem a trabalhar forçados causando a compressão e a inflamação da bursa.</p>

                        <p>Como Tratar: Nosso método consiste em tratar os causadores que estão acarretando a compressão da bursa, ou seja: os músculos, e a partir daí liberamos as possíveis contraturas e os tendões deixam de trabalhar forçados e bursa deixa de ser comprimida e com isso ocorre a eliminação da inflamação.</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>