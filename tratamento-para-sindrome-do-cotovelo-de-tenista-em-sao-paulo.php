<?php
$title       = "Tratamento para Síndrome do Cotovelo de Tenista em São Paulo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O cotovelo de tenista, ou epicondilite lateral, é causado pelo uso excessivo do cotovelo, muito comum no esporte do tênis. É uma lesão no tendão envolvendo os músculos extensores do antebraço, e está relacionado a altas cargas de treinamento e estresse físico por tempos prolongados na região. A fisioterapia é uma ótima alternativa de Tratamento para Síndrome do Cotovelo de Tenista em São Paulo.</p>
<p>Especialista no mercado, a Instituto da Dor é uma empresa que ganha visibilidade quando se trata de Tratamento para Síndrome do Cotovelo de Tenista em São Paulo, já que possui mão de obra especializada em Formigamento nas Pernas, Tratamento Dor Atrás do Joelho, Quiropraxia Manual, Dry Needling e Tratamento Cervicalgia. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Tratamentos terapêuticos, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>