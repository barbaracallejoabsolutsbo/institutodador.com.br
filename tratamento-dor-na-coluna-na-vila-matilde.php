<?php
$title       = "Tratamento Dor na Coluna na Vila Matilde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor, localizado na zona leste de São Paulo, atende casos de dores na coluna, dores articulares, musculares, entre outros. Agende um horário conosco para passar em consulta com um de nossos profissionais e tenha suporte especializado para Tratamento Dor na Coluna na Vila Matilde. Com preços especiais e atendimento Premium, suas dores e desconfortos serão resolvidos assim que diagnosticados.</p>
<p>Com a Instituto da Dor proporcionando o que se tem de melhor e mais moderno no segmento de Tratamentos terapêuticos consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Laserterapia, Clínica de Quiropraxia, Tratamento para Síndrome de Ciclista, Formigamento nas Pernas e Clínica de Acupuntura nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Tratamento Dor na Coluna na Vila Matilde.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>