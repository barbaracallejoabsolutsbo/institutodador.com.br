<?php
$title       = "Home Care no Parque do Carmo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conheça o Instituto da Dor, uma clínica que presta serviços especializados de fisioterapia, quiropraxia, tratamentos de lesões por esforço, problemas musculares, articulares, entre outros. Aqui você tem serviço de Home Care no Parque do Carmo acessível para todas essas especialidades acima citadas e ainda mais. Consulte nosso catálogo de serviços e entre em contato para mais informações.</p>
<p>A Instituto da Dor além de ser uma das principais empresas do setor de Tratamentos terapêuticos tem como foco trazer o que se tem de melhor nesse ramo. E por ser uma excelente empresa, se dispõe a prestar uma ótima assessoria, tanto para Home Care no Parque do Carmo, quanto para Clínica de Fisioterapia, Quiropraxia Instrumental TIQ, Tratamento Dor Sacral, Tratamento Artrite e Tratamento Dores nos Joelhos. Conte com a gente, pois temos uma equipe de sucesso esperando pelo seu contato.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>