<?php
$title       = "Clínica de Acupuntura em Engenheiro Goulart";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para vivermos bem, é muito importante que cuidemos de nossa saúde e bem estar. A Clínica Instituto da Dor realiza tratamentos terapêuticos diversos, entre eles você encontra a técnica Shian, acupuntura, entre outros, liberando a tensão das fibras musculares, causando sensação de relaxamento e melhorando problemas de dores e desconfortos musculares e articulares. Agende seu horário em nossa Clínica de Acupuntura em Engenheiro Goulart.
</p>
<p>Sendo referência no ramo Tratamentos terapêuticos, garante o melhor em Clínica de Acupuntura em Engenheiro Goulart, a empresa Instituto da Dor trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Tratamento Dor Atrás do Joelho, Tratamento Cervicalgia, Dor no Pescoço, Ciático Inflamado e Tratamento para Escoliose para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>