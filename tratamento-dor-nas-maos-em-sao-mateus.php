<?php
$title       = "Tratamento Dor nas Mãos em São Mateus";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Instituto da Dor é referência em tratamentos relacionados à fisioterapia. Aqui, o Tratamento Dor nas Mãos em São Mateus é feito com massagens, alongamentos, liberação miofascial, quiropraxia, acupuntura, fisioterapia, entre outros procedimentos. Isso porque acreditamos no potencial físico de regeneração e na fisioterapia. Se o seu quadro patológico permitir, evitamos o uso de qualquer tipo de medicamento.</p>
<p>Como uma especialista em Tratamentos terapêuticos, a Instituto da Dor se destaca dentre as demais empresas quando o assunto é Tratamento Dor nas Mãos em São Mateus, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Tratamento para Síndrome do Cotovelo de Tenista, Tratamento Ciatalgia, Tratamento Dores nas Costas, Tratamento para Hérnia de Disco e Formigamento nas Pernas. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>