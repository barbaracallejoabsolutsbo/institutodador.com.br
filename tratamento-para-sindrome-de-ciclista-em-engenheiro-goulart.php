<?php
$title       = "Tratamento para Síndrome de Ciclista em Engenheiro Goulart";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A síndrome do ciclista é um tipo de síndrome gerada por pressão na região do períneo, local que fica apoiado sobre o banco da bicicleta, causando compressão nervosa em aproximadamente 60 a 70% dos ciclistas habituais. Essa compressão acaba irritando e inflamando a região causando alteração na movimentação, sensibilidade e problemas vasculares na região. Consulte nosso Tratamento para Síndrome de Ciclista em Engenheiro Goulart.</p>
<p>Com uma alta credibilidade no mercado de Tratamentos terapêuticos, proporcionando com qualidade, viabilidade e custo x benefício em Tratamento para Síndrome de Ciclista em Engenheiro Goulart, a empresa Instituto da Dor vem crescendo e mostrando seu potencial através, também de Tratamento para Dores de Cabeça, Quiropraxia Instrumental TIQ, Clínica de Acupuntura, Tratamento Dor na Coluna e Tratamento para Torcicolo, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>