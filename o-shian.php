<?php

    $title       = "O Shian";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "o-shian"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-pag">
            <h1><?php echo $h1;?></h1>
        </div>
        <div class="container">
            <div class="conteudo-pag">
                <div class="text-right">
                    <?php echo $padrao->breadcrumb(array($title)); ?>
                </div>
                <h2>O que é o SHIAN?</h2>
                <hr>
                <p>Aplicada há mais de três décadas no Brasil, a técnica foi passada de pai para filho e a sua principal característica é solucionar problemas musculares, tendinosos e articulares de forma breve e sem necessidade de incisões. O tratamento é aplicado por um fisioterapeuta ou massoterapeuta.</p>
                <p>O método consiste em liberar e alongar as fibras dos músculos e tendões para que esses voltem a dar total proteção às articulações.</p>
                <p>Na opinião do especialista em dores, Ademir Dorigo, não existe lesão que não possa ser trabalhada com o Shian. “Após um choque traumático, movimentos repetitivos, má postura, os tendões, os músculos ficam enrijecidos, e as articulações com movimentos limitados, são nesses casos que o Shian pode atuar nos pontos críticos do corpo. Manipulando músculos, articulações e tendões para impedir que esses atrofiem e percam mobilidade.”, esclarece Dorigo.</p>
                <p>A aplicação da técnica SHIAN é contra-indicada em casos de: trombose, problemas cardíacos, gestantes até o 4º mês de gestação, neoplasia.</p>
                <br>
                <h2>Benefícios da Técnica Shian - Liberação Miofascial</h2>
                <hr>
                <ul>
                    <li><i class="fas fa-check"></i> Ajuda a diminuir e/ou controlar o estresse</li>
                    <li><i class="fas fa-check"></i> Alivia tensões e rigidez musculares</li>
                    <li><i class="fas fa-check"></i> Promove uma recuperação mais rápida de contraturas musculares e lesões articulares</li>
                    <li><i class="fas fa-check"></i> Aumenta a flexibilidade e movimentos das articulações</li>
                    <li><i class="fas fa-check"></i> Reduz espasmo muscular</li>
                    <li><i class="fas fa-check"></i> Melhora a circulação sanguínea e o movimento dos fluidos linfáticos</li>
                    <li><i class="fas fa-check"></i> Normaliza a pressão arterial</li>
                    <li><i class="fas fa-check"></i> Ajuda no alívio de dores de cabeça, sobretudo, de origem tensional</li>
                    <li><i class="fas fa-check"></i> Melhora o estado e a nutrição da pele</li>
                    <li><i class="fas fa-check"></i> Corrige postura</li>
                    <li><i class="fas fa-check"></i> Fortalece o sistema imunológico</li>
                    <li><i class="fas fa-check"></i> Adquire maior concentração e desperta a criatividade</li>
                    <li><i class="fas fa-check"></i> Diminui o custo com assistência médica</li>
                    <li><i class="fas fa-check"></i> Provê benefícios emocionais</li>
                </ul>
                <br>
                <h2>Conhecendo a técnica SHIAN</h2>
                <hr>
                <p>Com a técnica SHIAN, você terá chance de conhecer e vivenciar uma nova filosofia, além de promover uma outra qualidade a sua vida e tratar suas disfunções musculares e/ou articulares.</p>
                <p>A técnica SHIAN, ajudará a reduzir drasticamente o consumo de medicamentos, pois ele trata os problemas das articulações, músculos e tendões em sua verdadeira origem. O domínio da técnica de manipulação representa um instrumento similar a uma caixa de pronto-socorro, ao alcance de suas mãos. A técnica SHIAN permite a integração do ser humano em seus aspectos: físico, mental e emocional.</p>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>