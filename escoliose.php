<?php

    $title       = "Escoliose";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/escoliose.jpg" alt="Escoliose">
                        
                        <p>Curvatura lateral da coluna, mais correto dizer, curvatura látero-lateral rotacional. Essa deformidade pode levar a anormalidades estruturais na pelve, vértebras e caixa torácica. Pode ocorrer nas regiões: cervical, torácica ou lombar da coluna.</p>
                        <p>Quando a escoliose for de ordem não estrutural ou funcional ela é causada por problemas musculares cervicotoracolombar.</p>                        
                        <p>Nossa técnica consiste em :</p>
                        <ul>
                            <li>Liberar as fibras musculares e possíveis contraturas /atrofias</li>
                            <li>Alongar os grupos musculares que foram liberados</li>
                            <li>Alinhar estrutura óssea</li>
                            <li>Correção postural</li>
                            <li>Com isso o problema está solucionado.</li>
                        </ul>
                        <h2>TRATAMENTO NÃO INVASIVO E NÃO MEDICAMENTOSO</h2>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>