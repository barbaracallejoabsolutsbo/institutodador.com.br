<?php
$title       = "Tratamento Cervicalgia na Bela Vista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A cervicalgia é um tipo de dor ou rigidez que ocorre no pescoço que pode causar enfraquecimento e inflamações nos músculos da região com o passar do tempo. Normalmente causado por erros posturais ou movimentos inadequados, a cervicalgia possui tratamento e pode ser encontrada conosco, no Instituto da Dor. Faça um agendamento para diagnóstico e Tratamento Cervicalgia na Bela Vista em nossa clínica.</p>
<p>Nós da Instituto da Dor trabalhamos dia a dia para garantir o melhor em Tratamento Cervicalgia na Bela Vista e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Tratamentos terapêuticos com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Tratamento Dor Sacral, Quiropraxia Instrumental TIQ, Clínica de Liberação Miofascial, Tratamento Cervicalgia e Tratamento Bursite e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>