<?php

    $title       = "Acupuntura";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "acupuntura"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>tratamentos-patologicos">
                            <h2>Quiropraxia</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-ativvo">
                        <a href="<?php echo $url; ?>tratamentos-preventivos">
                            <h2>Liberação Miofascial</h2>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ativvo">
                        <a href="<?php echo $url; ?>acupuntura">
                            <h2>Acupuntura</h2>
                        </a>
                    </div>
                </div> 
                <br>
                <h3>Histórico</h3>
                <hr>
                <p>A Acupuntura é uma terapia milenar chinesa e consiste no tratamento de enfermidades por meio da inserção de finas agulhas na pele em determinados locais do corpo. Ao longos dos anos a acupuntura tem mostrado sua eficácia principalmente no tratamento das dores, mas também auxilia no tratamento de diversas doenças.</p>

                <p>O tratamento de acupuntura do Instituto da Dor é realizado por um profissional especializado, com sessões que duram de 30 a 50 minutos.</p>

                <p>Entre as doenças tratáveis pela Acupuntura estão:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Artrites, Artroses, Bursites e Tendinites;
                    <li><i class="fas fa-check"></i> Enxaqueca, Labirintite e Tensão muscular;</li>
                    <li><i class="fas fa-check"></i> Gastrite e Prisão de Ventre;</li>
                    <li><i class="fas fa-check"></i> Ansiedade, Depressão, Estresse e Nervosismo.</li>
                </ul>
                <br>

                <p>Além da Acupuntura, no Instituto da Dor você também encontrará:</p>

                <br>
                <ul>
                    <li><i class="fas fa-check"></i> Liberação Miofascial;</li>
                    <li><i class="fas fa-check"></i> Quiropraxia;</li>
                    <li><i class="fas fa-check"></i> Laserterapia.</li>
                </ul>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>