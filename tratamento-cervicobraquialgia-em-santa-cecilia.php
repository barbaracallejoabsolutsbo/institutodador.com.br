<?php
$title       = "Tratamento Cervicobraquialgia em Santa Cecília";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você tem dores e desconfortos na região do pescoço que irradia para os ombros ou braços, sensações de choque, formigamento nas mãos ou dedos e perda de força dos membros, são altamente aconselháveis que procurasse auxílio profissional para diagnóstico e Tratamento Cervicobraquialgia em Santa Cecília. Agende um horário para consultar um profissional do Instituto da Dor e encontre solução para suas dores.</p>
<p>Se está procurando por Tratamento Cervicobraquialgia em Santa Cecília e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Instituto da Dor é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Tratamentos terapêuticos conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Tratamento Artrite, Clínica de Quiropraxia, Clínica de Liberação Miofascial, Tratamento para Síndrome do Manguito Rotador e Tratamento para Fascite Plantar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>