<?php
$title       = "Quiropraxia Instrumental TIQ na Cidade Tiradentes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Quiropraxia Instrumental TIQ na Cidade Tiradentes é um tipo de manipulação terapêutica feita com intervenção de instrumentos mecânicos que possuem uma força predefinida para corrigir disfunções e subluxações. Pode ser considerado um pouco mais confortável e seguro para o paciente, visto que os movimentos executados são auxiliados por maquinários próprios para esse tipo de terapia.
</p>
<p>Com a Instituto da Dor proporcionando o que se tem de melhor e mais moderno no segmento de Tratamentos terapêuticos consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Tratamento Dor no Ombro, Tratamento para Dor de Cabeça, Tratamento para Síndrome de Ciclista, Tratamento Dores no Trapézio e Ciático Inflamado nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Quiropraxia Instrumental TIQ na Cidade Tiradentes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>