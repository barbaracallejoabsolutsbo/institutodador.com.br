<?php
$title       = "Home Care no Belém";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Home Care no Belém é o nome que se dá ao serviço de atendimento e assistência médica domiciliar. Nesse tipo de atendimento, enfermeiros, médicos e especialistas vão até o paciente para atendimento na residência do próprio paciente. Uma ótima alternativa para idosos, deficientes, internação domiciliar, entre outros. Encontre Home Care no Belém com preço acessível perto de você. </p>
<p>Se está procurando por Home Care no Belém e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Instituto da Dor é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Tratamentos terapêuticos conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Tratamento para Dor, Tratamento para Dor de Cabeça, Quiropraxia Manual, Tratamento Cervicobraquialgia e Tratamento para Síndrome do Piriforme.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>