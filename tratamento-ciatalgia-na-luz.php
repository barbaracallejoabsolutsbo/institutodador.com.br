<?php
$title       = "Tratamento Ciatalgia na Luz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ciatalgia é o termo usado para denominar a dor que ocorre no nervo isquiático, que normalmente é causado por compressão nas raízes nervosas lombares. É um processo inflamatório que pode causar muita dor e desconforto e em alguns casos fraquezas e incapacidades. Procure Tratamento Ciatalgia na Luz com o Instituto da Dor e encontre soluções alternativas não medicamentosas para resolver suas dores físicas.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Instituto da Dor ganha destaque quando o assunto é Tratamentos terapêuticos. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Tratamento Ciatalgia na Luz até Tratamento para Esporão Calcâneo, Tratamento Artrose, Tratamento Dores na Coxa, Tratamento Dores nas Costas e Liberação Miofascial Manual com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>