<?php
$title       = "Liberação Miofascial Manual no Glicério";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Ambos processos têm o mesmo objetivo, a partir de pontos específicos, gerar pressão nos locais onde existem desconfortos e dores musculares. Na Liberação Miofascial Manual no Glicério, esses pontos de pressão são acessados com as mãos, com maior capacidade de controle por conta do tato. Em casos mais específicos são utilizados rolos, bolas e ganchos para acessar locais mais profundos e soltar a musculatura de forma completa.</p>
<p>A Instituto da Dor, como uma empresa em constante desenvolvimento quando se trata do mercado de Tratamentos terapêuticos visa trazer o melhor resultado em Liberação Miofascial Manual no Glicério para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Tratamento Dores na Lombar, Tratamento para Hérnia de Disco, Ciático Inflamado, Tratamento Bursite e Tratamento ATM para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>