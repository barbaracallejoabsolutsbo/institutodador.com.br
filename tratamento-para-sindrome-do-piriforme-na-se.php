<?php
$title       = "Tratamento para Síndrome do Piriforme na Sé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Normalmente afetando mais a perna direita, a síndrome do piriforme é uma patologia que acomete o nervo ciático na região em que passa pelo piriforme, causando dor e desconforto. Isso acontece, pois quando ocorre o pressionamento pelo músculo, o nervo inflamado reage e causa desconforto. No Instituto da Dor você encontra tudo sobre Tratamento para Síndrome do Piriforme na Sé.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de Tratamentos terapêuticos, a Instituto da Dor oferece a confiança e a qualidade que você procura quando falamos de Tratamento Dor na Coluna, Laserterapia, Dor no Pescoço, Tratamento para Escoliose e Tratamento Cervicobraquialgia. Ainda, com o mais acessível custo x benefício para quem busca Tratamento para Síndrome do Piriforme na Sé, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>