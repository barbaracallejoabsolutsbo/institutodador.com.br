<?php
$title       = "Tratamento Dor Ciático em Ermelino Matarazzo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está procurando Tratamento Dor Ciático em Ermelino Matarazzo, acaba de encontrar a melhor alternativa da Zona Leste de São Paulo. Localizados no Tatuapé, em local de fácil acesso, o Instituto da Dor conta com tratamentos especializados para área da fisioterapia e recuperação muscular, articular e óssea. Consulte-nos para mais informações e entre em contato para agendamentos de consultas e sessões.</p>
<p>Se você está em busca por Tratamento Dor Ciático em Ermelino Matarazzo conheça a empresa Instituto da Dor, pois somos especializados em Tratamentos terapêuticos e trabalhamos com variadas opções de produtos e/ou serviços para oferecer, como Tratamento para Fibromialgia, Tratamento Dores na Coxa, Tratamento Dores nos Joelhos, Tratamento Dor nas Mãos e Tratamento para Síndrome do Piriforme. Sabendo disso, entre em contato conosco e faça uma cotação, temos competentes profissionais para dar o melhor atendimento possível para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>