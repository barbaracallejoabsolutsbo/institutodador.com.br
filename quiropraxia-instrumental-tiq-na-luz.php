<?php
$title       = "Quiropraxia Instrumental TIQ na Luz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Quiropraxia Instrumental TIQ na Luz é um tipo de manipulação terapêutica feita com intervenção de instrumentos mecânicos que possuem uma força predefinida para corrigir disfunções e subluxações. Pode ser considerado um pouco mais confortável e seguro para o paciente, visto que os movimentos executados são auxiliados por maquinários próprios para esse tipo de terapia.
</p>
<p>Como uma empresa especializada em Tratamentos terapêuticos proporcionamos sempre o melhor quando falamos de Tratamento para Síndrome de Ciclista, Tratamento para Dor, Tratamento para Dor de Cabeça, Clínica de Acupuntura e Tratamento Ciatalgia. Com potencial necessário para garantir qualidade e excelência em Quiropraxia Instrumental TIQ na Luz com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Instituto da Dor trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>