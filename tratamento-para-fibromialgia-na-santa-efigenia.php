<?php
$title       = "Tratamento para Fibromialgia na Santa Efigênia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O diagnóstico para a fibromialgia é clínico, ou seja, se for feita uma entrevista clínica de forma atenciosa, já é possível obter diagnóstico para assim optar pelo Tratamento para Fibromialgia na Santa Efigênia mais adequado para você. Agende um horário para avaliação clínica profissional e encontre soluções para problemas de fibromialgia. Estamos localizados na Zona Oeste de São Paulo, no bairro do Tatuapé.</p>
<p>Tendo como especialidade Tratamento para Síndrome do Cotovelo de Tenista, Tensão Muscular, Tratamento Cervicobraquialgia, Quiropraxia Manual e Tratamento para Síndrome do Túnel do Carpo, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Tratamentos terapêuticos. Por isso, quando falamos de Tratamento para Fibromialgia na Santa Efigênia, buscar pelos membros da empresa Instituto da Dor é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>