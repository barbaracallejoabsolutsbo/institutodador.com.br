<?php
$title       = "Tratamento Tendinite no Bom Retiro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A tendinite é um processo inflamatório que ocorre nos tendões, normalmente na região do pulso, causado por esforço repetitivo diário ocasionado pelo trabalho. Pessoas que trabalham em escritórios, com maquinário de indústrias, escrevendo muito ou movimentando muito o pulso costumam ter tendinite. No Instituto da Dor você encontra fisioterapia e Tratamento Tendinite no Bom Retiro com preço acessível.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Instituto da Dor ganha destaque quando o assunto é Tratamentos terapêuticos. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Tratamento Tendinite no Bom Retiro até Clínica de Fisioterapia, Tratamento Dor no Ombro, Lesão por Esforço Repetitivo, Tratamento Dor nas Mãos e Tratamento para Torcicolo com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>