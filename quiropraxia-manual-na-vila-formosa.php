<?php
$title       = "Quiropraxia Manual na Vila Formosa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sendo uma grande aliada da fisioterapia, a Quiropraxia Manual na Vila Formosa é uma prática que proporciona excelentes resultados quanto a melhoria em fortalecimentos na coluna e sistema nervoso em processos de recuperação. Auxiliando na melhoria de movimentos diversos e recuperando aos poucos a mobilidade e flexibilidade do paciente, a quiropraxia é muito indicada para tendinites, dores e incômodos articulares.</p>
<p>Conseguindo ofertar Tratamento para Fibromialgia, Tratamento Dores nas Costas, Laserterapia, Liberação Miofascial Manual e Tratamento para Torcicolo a Instituto da Dor proporciona Quiropraxia Manual na Vila Formosa ideal para atender às reais necessidades de seus clientes e parceiros, assim comprovando o fato de ser a empresa mais completa e eficiente do mercado de Tratamentos terapêuticos. Unindo a experiência que nossa empresa traz com profissionais competentes conseguimos proporcionar o melhor para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>