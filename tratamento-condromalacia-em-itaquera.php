<?php
$title       = "Tratamento Condromalácia em Itaquera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Visando fortalecer estruturas relacionadas à patela e reduzir a pressão sobre a mesma, o Tratamento Condromalácia em Itaquera ou de condropatia visa aliviar a dor e desconforto na região do joelho causada por processos inflamatórios degenerativos da patela. Por meio de sessões de fisioterapia e tratamentos alternativos, são fortalecidas as estruturas que dão suporte ao joelho a fim de reduzir a pressão sob a patela.</p>
<p>Entre em contato com a Instituto da Dor se você busca por Tratamento Condromalácia em Itaquera. Somos uma empresa especializada com foco em Tratamento Dor na Coluna, Tratamento para Síndrome do Manguito Rotador, Tensão Muscular, Clínica de Acupuntura e Tratamento Dor Ciático onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de Tratamentos terapêuticos. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>