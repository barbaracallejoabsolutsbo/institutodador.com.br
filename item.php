<?php

    $title       = "Página de palavra chave padrão";
    $description = "Inserir descrição da palavra chave."; 
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $url_title   = $padrao->formatStringToURL($title);
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </div>
                <div class="col-md-9 text-justify">
                    <h1 class="main-title"><?php echo $h1; ?></h1>
                    <p>Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito.</p>
                    <h2>SUB-TITULO</h2>
                    <p>Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito.</p>
                    <p>Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito. Inserir o texto de palavra-chave. Lembrando que a palavra-chave tem que ficar em negrito.</p>
                    <?php include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
            </div>
            <hr>
            <?php include "includes/veja-tambem.php"; ?>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.padrao.keyword"
    )); ?>
    
</body>
</html>