<?php

    $title       = "Quem somos";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/flexslider",
        "tools/fancybox",
        "galeria-fotos",
        "a-clinica"
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container text-justify">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="a-clinica">
                <h1>Quem somos</h1>
                <hr>
                <p>Somos o Instituto da Dor, clínica especializada em solucionar suas dores. Com mais de 36 anos de experiência, realizamos tratamentos terapêuticos aplicando acupuntura, quiropraxia e uma técnica exclusiva de liberação miofascial, passada de pai para filho há 3 gerações.</p>
                <p>A principal característica de nossos tratamentos é solucionar problemas musculares, tendíneos e articulares de forma breve, sem necessidade de incisões.</p>
                <p>Toda a credibilidade que a empresa possui é fruto de muita dedicação, além da contínua busca pela excelência. Nosso objetivo é apenas um: proporcionar eficácia e qualidade nos tratamentos executados, garantindo sempre bem-estar e satisfação aos nossos pacientes.</p>
                <p>Oferecemos os seguintes tratamentos:</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h2>Patologias que tratamos</h2>
                        <p>Artrite, Artrose, Bursite, Capsulite articular, Cervicalgia / Cervicobraquialgia, Ciatalgia, Epicondilite, Escoliose, Esporão Calcâneo, Fibromialgia, Hérnia de Disco, Hipercifose, Lombalgia, Manguito Rotador, Tendinite,Túnel do carpo.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
                </div>
            </div>
            <?php include "includes/carrossel.php"; ?>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/jquery.flexslider-min",
        "tools/jquery.fancybox"
        
    )); ?>
    <script>
        $(function(){            
            // Basic Carousel
            $("#basic-carousel").flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5
            });
        });
    </script>
    
</body>
</html>