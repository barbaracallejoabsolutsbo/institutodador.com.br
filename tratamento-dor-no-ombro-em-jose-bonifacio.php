<?php
$title       = "Tratamento Dor no Ombro em José Bonifácio";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você quer encontrar o melhor local da Zona Leste para Tratamento Dor no Ombro em José Bonifácio, conheça o Instituto da Dor. Há mais de 35 anos atuando no mercado de fisioterapia e terapias alternativas não medicamentosas, tratamos diversos problemas clínicos musculares e articulares causados por posturas e esforços físicos inadequados ou processos inflamatórios patológicos. Consulte-nos para diagnósticos e sessões de tratamento.</p>
<p>Pensando em proporcionar a todos os clientes o melhor em métodos quando se trata de Tratamentos terapêuticos? A empresa Instituto da Dor vem ganhando destaque em referência no assunto de Tratamento Dor no Ombro em José Bonifácio. Por isso, solicite um orçamento para Tratamento Artrose, Formigamento nas Pernas, Tratamento para Síndrome de Ciclista, Tratamento Dores na Coxa e Tratamento Artrite assim você contará com a qualidade que somente a nossa empresa oferece para todos os seus clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>