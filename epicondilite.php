<?php

    $title       = "Epicondilite";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/epicondilite.jpg" alt="Epicondilite">
                        <p>A epicondilite, também chamada de “cotovelo do tenista” (tennis elbow), é uma inflamação ou irritação de origem mecânica. Situa-se nas adjacências de uma saliência óssea (epicôndilo) que fica na face lateral ou externa do cotovelo, onde se originam os músculos que levantam (extendem) o punho e os dedos em direção às costas da mão, ou acometem a face medial desta saliência, no qual encontram-se os tendões dos músculos que flexionam o antebraço (cotovelo de golfista).</p>
                        <p>Causadas por movimentos repetitivos gerando um encurtamento da musculatura do antebraço fazendo com que os tendões trabalhem forçados e conseqüentemente causando inflamação.</p>
                        <p>Como Tratar: A nossa técnica vai direto ao causador que são os músculos. Liberando a musculatura, automaticamente os tendões ficam livres e a inflamação cessa dando fim a epicondilite.</p>
                        <h2>OBS: TRATAMENTO NÃO INVASIVO E NÃO MEDICAMENTOSO</h2>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>