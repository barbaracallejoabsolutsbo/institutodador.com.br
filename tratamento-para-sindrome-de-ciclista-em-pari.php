<?php
$title       = "Tratamento para Síndrome de Ciclista em Pari";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Uma das formas de prevenir a síndrome do ciclista é configurar corretamente a distância do seu assento em relação a altura do pedal, de forma que quando pedale não haja pressão ou desconforto na região do períneo. Com exercícios específicos e técnicas de fisioterapia é possível prevenir e amenizar bastante os impactos dessa síndrome. Faça Tratamento para Síndrome de Ciclista em Pari no Instituto da Dor.</p>
<p>Na busca por uma empresa referência, quando o assunto é Tratamentos terapêuticos, a Instituto da Dor será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Tratamento Dor Sacral, Tratamento para Torcicolo, Tratamento para Dores de Cabeça, Tratamento Condromalácia e Tratamento Dor Atrás do Joelho, oferece Tratamento para Síndrome de Ciclista em Pari com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>