<?php
$title       = "Laserterapia em Itaim Paulista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Laserterapia em Itaim Paulista é um tipo de terapia feita com laser de baixa potência e que atende a vários ramos clínicos. Muito útil para tratar lesões musculares, estimular recuperação e cicatrização tecidual. Popularmente utilizado também para tratar processos inflamatórios e dores musculares, o laser de baixa potência muda no estado Redox celular em suas relações com o oxigênio.</p>
<p>Com a Instituto da Dor proporcionando o que se tem de melhor e mais moderno no segmento de Tratamentos terapêuticos consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Tratamento Dor Ciático, Tratamento Dores nos Joelhos, Tratamento Cervicobraquialgia, Ciático Inflamado e Tratamento Dores na Coxa nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Laserterapia em Itaim Paulista.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>