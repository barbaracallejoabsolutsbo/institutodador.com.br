<?php
$title       = "Tratamento para Síndrome do Manguito Rotador na Bela Vista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No Instituto da Dor você encontra Tratamento para Síndrome do Manguito Rotador na Bela Vista com profissionais especializados. Por meio de sessões de fisioterapia visando melhorar a flexibilidade, fortalecimento da musculatura, circulação e reparação tecidual, os ganhos e benefícios em relação a movimentação e redução de dor são altamente satisfatórios. Agende uma consulta para tratamentos e procedimentos.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Tratamentos terapêuticos, a Instituto da Dor ganha destaque por ser confiável e idônea quando falamos não só de Tratamento para Síndrome do Manguito Rotador na Bela Vista, mas também quando o assim é Tratamento para Torcicolo, Tratamento Ciatalgia, Clínica de Acupuntura, Tratamento para Esporão Calcâneo e Dor no Pescoço. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>