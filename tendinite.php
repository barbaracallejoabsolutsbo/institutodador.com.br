<?php

    $title       = "Tendinite";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tratamentos-patologicos-todos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <div class="pag-procedimentos">
                <h1><?php echo $h1;?></h1>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php include "includes/menu-patologicos.php"; ?>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="<?php echo $url; ?>imagens/tratamentos-patologicos/tendinite.jpg" alt="Tendinite">
                        <p>Tendinite é uma síndrome de excesso de uso em resposta a inflamação local devido a microtraumas repetidos que podem ocorrer devido a desequilíbrios musculares ou fadiga, alterações nos exercícios ou nas rotinas funcionais, erros de treinamento ou uma combinação de vários desses fatores.</p>

                        <p>Devido ao repetitivo uso de um determinado grupo muscular, ocorre uma inflamação nos tendões levando a uma tendinite. A mais comum é tendinite no braço, em função do excesso de digitação.</p>

                        <p>Como Tratar: O nosso tratamento revolucionário atua diretamente nos causadores que são os músculos do braço, evitando que os tendões trabalhem forçados e prevenindo a inflamação (tendinite).</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>