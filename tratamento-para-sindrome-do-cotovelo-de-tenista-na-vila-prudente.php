<?php
$title       = "Tratamento para Síndrome do Cotovelo de Tenista na Vila Prudente";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre procedimentos de fisioterapia e terapia alternativa a fim de reduzir a inflamação e dor ocasionado pela síndrome do cotovelo do tenista. O Instituto da Dor trabalha com sessões de fisioterapia, acupuntura, laserterapia, ondas de choque, compressas, fortalecimento e alongamento, a fim de prevenir e reduzir os danos. Faça Tratamento para Síndrome do Cotovelo de Tenista na Vila Prudente conosco.</p>
<p>Nós da Instituto da Dor trabalhamos dia a dia para garantir o melhor em Tratamento para Síndrome do Cotovelo de Tenista na Vila Prudente e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Tratamentos terapêuticos com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Tratamento para Torcicolo, Tratamento Condromalácia, Clínica de Fisioterapia, Tratamento Dores nos Joelhos e Tratamento Ciatalgia e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>