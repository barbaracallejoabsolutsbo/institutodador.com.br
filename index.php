<?php

    $h1      	 = "Home";
    $title    	 = "Tratamentos Terapêuticos";
    $description = "Tratamentos Terapêuticos"; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "tools/flexslider",
        "galeria-fotos",
        "home"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-home.php"; ?>


    
    <main class="main-content">
        <div class="banner-home">
            <div class="theme-default-nivo-slider">
                <div id="slider" class="nivoSlider"> 
                    <img src="<?php echo $url; ?>imagens/banner.jpg" alt="Imagem" title="#01">
                </div>
                <div id="01" class="nivo-html-caption">
                    <h1>Instituto da Dor</h1>
                    <p>Aqui a sua dor tem jeito!</p>
                </div>
            </div>
        </div>
        <div class="atendimento-home">
            <div class="container">
                <div class="row text-center">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <img src="<?php echo $url; ?>imagens/shain.jpg" class="img-responsive" alt="Acupuntura" title="Acupuntura">
                        <h3>Acupuntura</h3>
                        <p>A Acupuntura consiste no tratamento de patologias por meio da inserção de finas agulhas na pele em determinados locais do corpo.</p>
                        <a class="btn-arendimento" href="<?php echo $url; ?>acupuntura">Saiba mais</a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <img src="<?php echo $url; ?>imagens/fisio.jpg" class="img-responsive" alt="Liberação miofascial" title="Liberação miofascial">
                        <h3>Liberação Miofascial</h3>
                        <p>Todo tratamento aplicado pela Instituto da Dor é feito sem interação medicamentosa, respeitando exclusivamente o uso de remédios por indicação médica.</p>
                        <a class="btn-arendimento" href="<?php echo $url; ?>tratamentos-preventivos">Saiba mais</a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 antedimentos-inicial">
                        <img src="<?php echo $url; ?>imagens/tratamentos.jpg" class="img-responsive" alt="Quiropraxia" title="Quiropraxia">
                        <h3>Quiropraxia</h3>
                        <p>A Quiropraxia consiste em uma técnica com foco na coluna vertebral. Esta terapia manual pode ajudar a tratar problemas relacionados a articulações, músculos, ossos, tendões e ligamentos.</p>
                        <a class="btn-arendimento" href="<?php echo $url; ?>tratamentos-patologicos">Saiba mais</a>
                    </div>
                </div>
            </div>
        </div>
        <div  class="faixa-contato">
            <div class="container">
                <a href="https://api.whatsapp.com/send?phone=5511973167229&text=Ol%C3%A1%2C%20nossa%20unidade%20se%20encontra%20no%20endere%C3%A7o%20Rua%20Visconde%20de%20Itabora%C3%AD%2C%20171%20-%20Tatuap%C3%A9%20-%20S%C3%A3o%20Paulo%20-%20SP">
                    <h2>Entre em contato e tire suas dúvidas pelo WhatsApp <i class="fab fa-whatsapp"></i></h2>
                </a>
            </div>
        </div>
        <div class="sobre-nos">
            <div class="container">
                <h2>Instituto da Dor</h2>
                <hr>
                <p>O Instituto da Dor é uma clínica que realiza tratamentos terapêuticos aplicando acupuntura, quiropraxia e a técnica de liberação miofascial exclusiva, visando tratamentos patológicos e preventivos.</p>
                <p>A principal característica de nossa técnica é solucionar problemas musculares, tendíneos e articulares de forma breve e sem necessidade de incisões, por exemplo: artrite, bursite, ciatalgia, epicondilite, escoliose, esporão calcâneo, fibromialgia, hiperlordose, hipercifose, hérnia de disco, lombalgia, síndrome do túnel do carpo, tendinite, entre outros</p>
                <?php include "includes/carrossel.php"; ?>
                <br>
                <a class="btn-empresa" href="<?php echo $url; ?>a-clinica">Saiba mais sobre nós</a>
            </div>
        </div>
        <div class="contador-home">
            <div class="efeito">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <h3>+ de <strong class="counter">15.569</strong></h3>
                            <p>Clientes Atendidos</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="tratamentos">
            <div class="container">
                <h2>Depoimentos</h2>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="content-image">
                            <p>“Profissional extremamente qualificado,faz uma avaliação completa e orienta o melhor tratamento”</p>
                            <h5>- Karen Joana</h5>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="content-image">
                            <p>“Lugar excepcional! Tem me ajudado muito no tratamento de minhas dores crônicas. Pessoas profissionais e qualificadas!”</p>
                            <h5>- Antonio Rocha</h5>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="content-image">
                            <p>“Excelente local para tratamentos de dores musculares e do corpo em geral. Equipe sensacional. Muito solícita e prestativa.”</p>
                            <h5>- Mari Bouchardet</h5>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="content-image">
                            <p>“Sensacional! Estou com uma hérnia de disco que me trava e causa dores horríveis, saí sem dores e andando normalmente. Parabéns Instituto da Dor!”</p>
                            <h5>- Cynthia Paiva</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.nivo",
        "tools/jquery.flexslider-min",
        "tools/jquery.fancybox",
        "tools/contador-clientes",
        "tools/jquery.slick"
    )); ?>

    <script>
        $(function(){
            $(".counter").counterUp({
                delay: 100,
                time:7000
            });
        });
    </script>

    <script>
        $(function(){
            $(".autoplay").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000
            });
        });
    </script>

    <script>
        $(function(){
            $("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });

        $(function(){
           function scrollTopMenu(a){
               var id_element = $(a).attr("data-id");
               var top = $("#"+id_element).offset().top -150;
               if(id_element === "home")
               {
                   var top = 0;
               }
               $("html, body").animate({
                   scrollTop: top
               }, 700);
           }
           $("ul li a").click(function(){
               scrollTopMenu($(this));
           });
           $("a.click-second").click(function(){
               scrollTopMenu($(this));
           });
        });
    </script>

    <script>
        $(function(){            
            // Basic Carousel
            $("#basic-carousel").flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5
            });
        });
    </script>
    
</body>
</html>