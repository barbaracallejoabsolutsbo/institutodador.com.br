<?php 

    

    class classPadrao {
        
        public $url = "";
        
        public $canonical = "";
        
        public $breadcrumb_url_home = "";
        
        public $breadcrumb_text_before_home = "";
        
        public $breadcrumb_text_home = "Home";
        
        public $breadcrumb_spacer = " » ";
        
        public $url_extension = "";
        
        public $token = "";
        
        public $tags_status = true;
        
        public $assets_dir = "assets/";
        
        public $p_url_home = "";

        public $p_url_contact = "";
        
        private $css_compress = "";
        
        public $css_files_default = array();
        
        private $js_compress = "";
        
        public $js_files_default = array();
        
        private $url_validation_token = "http://www.qsmi.net.br/Z_CheckToken/check/";
        
        private $db_type = "mysql";
        
        private $db_name = "qsminet_sistema";
        
        private $db_host = "qsmi.net.br";
        
        private $db_user = "qsminet_token1";
        
        private $db_passwd = "86B4Q7DcxwXd";
        
        /**
         * Define as propriedades url e canonical
         * @param Array $array_urls
         */
        public function __construct($array_urls, $options = null)
        {
            if(!empty($options))
            {
                foreach($options as $i => $option)
                {
                    $this->$i = $option;
                }
            }
            if(isset($options["token"]))
            {
                $this->checkToken();
                $this->url_extension = ".php";
                $this->assets_dir = "_q/";
                if(empty($options["p_url_home"]))
                {
                    $this->alerta("Parâmetro vazio: p_url_home");
                }
                else
                {
                    $this->breadcrumb_url_home = $options["p_url_home"];
                }
                if(empty($options["p_url_contact"]))
                {
                    $this->alerta("Parâmetro vazio: p_url_contact");
                }
            }
            $this->url = $this->setUrl($array_urls);
            $this->canonical = $this->setCanonical();
        }
        
        private function setUrl($urls)
        {
            $url = $urls[1];
            if(strpos($_SERVER["SERVER_NAME"], "localhost") !== false)
            {
                $url = $urls[0];
            }
            return $url;
        }
        
        private function setCanonical()
        {
            $pag = $_SERVER["PHP_SELF"];
            $url_pagina = end((explode("/", $pag)));
            if($url_pagina == "index.php" || $url_pagina == "index" || $url_pagina == "")
            {
                $url_pagina = str_replace("index.php", "", $url_pagina);
                $url_canonical = $this->url.$url_pagina;
            }
            else
            {
                $url_pagina = str_replace(".php", "", $url_pagina);
                $url_canonical = $this->url.$url_pagina;
            }
            if(!empty($this->url_extension))
            {
                $url_canonical .= $this->url_extension;
            }
            return $url_canonical;
        }

        /**
         * Formata uma string em uma url amigável
         * @param String $string
         */
        public function formatStringToURL($string)
        {
            $str   = trim($string);
            $str_a = strtolower($str);
            $str_b = strip_tags($str_a);
            $str_c = preg_replace_callback('!\s+!',
            function($m){
                return "-";
            }, $str_b);
            $array_a = array('.', '!', '@', '#', '$', '%', '&', '*', '+', '=', '(', ')', '[', ']', '{', '}', '<', '>', '\'');
            $array_b = array('-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
            $str_d = str_replace($array_a, $array_b, $str_c);
            $array_1 = array(',', ';', ':', '/', '~', '?', '!', 'á', 'é', 'í', 'ó', 'ú', 'â', 'ê', 'î', 'ô', 'û', 'à', 'è', 'ì', 'ò', 'ù', 'ã', 'õ', 'ç', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Â', 'Ê', 'Î', 'Ô', 'Û', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ã', 'Õ', 'Ç', 'Ñ', 'ä', 'Ä', 'ë', 'Ë', 'ï', 'Ï', 'ö', 'Ö', 'ü', 'Ü');
            $array_2 = array('', '', '', '-', '-', '', '', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'o', 'c', 'n', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'o', 'c', 'n', 'a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'u', 'u');
            $str_e = str_replace($array_1, $array_2, $str_d);
            $str_f = preg_replace_callback('/[-]+/',
            function($m){
                return "-";
            }, $str_e);
            $url = trim($str_f, "-");
            return $url;
        }

        /**
         * Forma uma lista de links
         * @param Array $array - Recebe uma lista de palavras chave
         * @param Array $set_options - Recebe os parametros opicionais
         */
        public function subMenu($array, $set_options = null)
        {
            $html = "";
            $options = array(
                "id" => empty($set_options["id"]) ? "" : $set_options["id"],
                "class" => empty($set_options["class"]) ? "" : $set_options["class"],
                "random" => empty($set_options["random"]) ? false : true,
                "limit" => empty($set_options["limit"]) ? 9999 : $set_options["limit"]
            );
            $class = !empty($options["class"]) ? " class=\"" . $options["class"] . "\"" : "";
            if($options["random"] == true){
                shuffle($array);
            }
            $i = 1;
            foreach ($array as $chave => $valor) {
                $palavra = $valor;
                if(!is_numeric($chave)){
                    $palavra = $chave;
                }
                if($i <= $options["limit"]){
                    $url_palavra = $this->formatStringToURL($palavra);
                    if(!empty($this->url_extension))
                    {
                        $url_palavra .= $this->url_extension;
                    }
                    $html .= "<li" . (!empty($options["id"]) ? " id=\"" . $options["id"] . "-" . $i . "\" " : "") . $class . ">";
                    $html .= "<a href=\"" . $this->url . $url_palavra . "\" title=\"" . $palavra . "\">" . $palavra . "</a></li>\n";
                    $i++;
                }
            }
            return $html;
        }

        /**
         * Forma uma lista de Thumbs
         * @param Array $array - Recebe uma lista de palavras chave
         * @param Array $set_options - Recebe os parametros opicionais
         */
        public function listaThumbs($array, $set_options = null)
        {
            $html = "";
            $options = array(
                "id" => empty($set_options["id"]) ? "" : $set_options["id"],
                "class_div" => empty($set_options["class_div"]) ? "col-md-3" : $set_options["class_div"],
                "class_section" => empty($set_options["class_section"]) ? "" : $set_options["class_section"],
                "class_img" => empty($set_options["class_img"]) ? "img-responsive" : $set_options["class_img"],
                "title_tag" => empty($set_options["title_tag"]) ? "h2" : $set_options["title_tag"],
                "folder_img" => empty($set_options["folder_img"]) ? "imagens/thumbs/" : $set_options["folder_img"],
                "extension" => empty($set_options["extension"]) ? "jpg" : $set_options["extension"],
                "limit" => empty($set_options["limit"]) ? 9999 : $set_options["limit"],
                "type" => empty($set_options["type"]) ? 1 : $set_options["type"],
                "random" => empty($set_options["random"]) ? false : true,
                "text" => empty($set_options["text"]) ? false : true,
                "headline_text" => empty($set_options["headline_text"]) ? "Veja Mais" : $set_options["headline_text"],
            );
            if($options["random"] === true){
                $nova_array = array();
                foreach ($array as $chave => $valor) {
                    $nova_array[] = array("chave" => $chave, "valor" => $valor);
                }
                shuffle($nova_array);
                $array = array();
                foreach ($nova_array as $valor) {
                    $array[$valor["chave"]] = $valor["valor"];
                }
            }
            $class_div = !empty($options["class_div"]) ? " class=\"" . $options["class_div"] . "\"" : "";
            $class_section = !empty($options["class_section"]) ? " class=\"" . $options["class_section"] . "\"" : "";
            $class_img = !empty($options["class_img"]) ? " class=\"" . $options["class_img"] . "\"" : "";
            $img_folder = $options["folder_img"];
            $i = 1;
            foreach ($array as $chave => $valor)
            {
                $palavra = $valor;
                $text = "";
                if(!is_numeric($chave)){
                    $palavra = $chave;
                    $text = $valor;
                }
                if($i <= $options["limit"]){
                    $src_img = $this->verificaImagem($this->url . $img_folder . $this->formatStringToURL($palavra) . "." . $options["extension"]);
                    $url_page = $this->url . $this->formatStringToURL($palavra);
                    if(!empty($this->url_extension))
                    {
                        $url_page .= $this->url_extension;
                    }
                    switch ($options["type"]){
                        case 3:
                            $html .= "<div" . (!empty($options["id"]) ? " id=\"".$options["id"] . "-" . $i . "\" " : "") . $class_div . ">\n";
                            $html .= "<div class=\"icon-block viewport\">\n";
                            $html .= "<a rel=\"nofollow\" href=\"" . $url_page . "\" title=\"" . $palavra . "\">\n";
                            $html .= "<div class=\"dark-background\">\n";
                            $html .= "<" . $options["title_tag"] . ">\n";
                            $html .= $palavra . "\n";
                            $html .= "</" . $options["title_tag"] . ">\n";
                            $html .= "</div>\n";
                            $html .= "<img src=\"" . $src_img . "\" alt=\"" . $palavra . "\" title=\"" . $palavra . "\" " . $class_img . ">\n";
                            $html .= "</a>\n";
                            $html .= "</div>\n";
                            if($options["text"] === true && !empty($text)){
                                $html .= "<p>" .$text . "</p>\n";
                            }
                            $html .= "</div>\n";
                            break;
                        case 2:
                            $html .= "<div" . (!empty($options["id"]) ? " id=\"".$options["id"] . "-" . $i . "\" " : "") . $class_div . ">\n";
                            $html .= "<div class=\"icon-block viewport\">\n";
                            $html .= "<a rel=\"nofollow\" href=\"" . $url_page . "\" title=\"" . $palavra . "\">\n";
                            $html .= "<div class=\"dark-background\">\n";
                            $html .= "<em>" . $options["headline_text"] . "</em>\n";
                            $html .= "</div>\n";
                            $html .= "<img src=\"" . $src_img . "\" alt=\"" . $palavra . "\" title=\"" . $palavra . "\" " . $class_img . ">\n";
                            $html .= "</a>\n";
                            $html .= "</div>\n";
                            $html .= "<" . $options["title_tag"] . ">\n";
                            $html .= "<a href=\"". $url_page . "\" title=\"" . $palavra . "\">" . $palavra . "</a>\n";
                            $html .= "</" . $options["title_tag"] . ">\n";
                            if($options["text"] === true && !empty($text)){
                                $html .= "<p>" .$text . "</p>\n";
                            }
                            $html .= "</div>\n";
                            break;
                        default:
                            $html .= "<div" . (!empty($options["id"]) ? " id=\"".$options["id"] . "-" . $i . "\" " : "") . $class_div . ">\n";
                            $html .= "<section". $class_section .">\n";
                            $html .= "<a rel=\"nofollow\" href=\"" . $url_page . "\" title=\"" . $palavra . "\">\n";
                            $html .= "<img src=\"" . $src_img . "\" alt=\"" . $palavra . "\" title=\"" . $palavra . "\" " . $class_img . ">\n";
                            $html .= "</a>\n";
                            $html .= "<" . $options["title_tag"] . ">\n";
                            $html .= "<a href=\"". $url_page . "\" title=\"" . $palavra . "\">" . $palavra . "</a>\n";
                            $html .= "</" . $options["title_tag"] . ">\n";
                            $html .= "</section>\n";
                            $html .= "</div>\n";
                            break;
                    }
                    $i++;
                }
            }
            return $html;
        }

        /**
         * Forma o breadcrumb
         * @param Array $array - Recebe uma lista de palavras chave
         * @param Array $set_options - Recebe os parametros opicionais
         */
        public function breadcrumb($array)
        {
            $html = "";
            $n_array = count($array);
            $link_home = $this->url;
            if(!empty($this->breadcrumb_url_home))
            {
                $link_home = $this->breadcrumb_url_home;
            }
            $html .= "<div id=\"breadcrumb\" itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">\n";
            $html .= $this->breadcrumb_text_before_home;
            $html .= "<a rel=\"home\" href=\"" . $link_home . "\" title=\"" . $this->breadcrumb_text_home . "\" itemprop=\"url\"><span itemprop=\"title\">" . $this->breadcrumb_text_home . "</span></a>" . $this->breadcrumb_spacer;
            $i = 1;
            foreach($array as $string){
                $html .= $this->setBreadcrumb($i, $n_array, $string);
                if($i < $n_array){
                    $html .= $this->breadcrumb_spacer;
                }
                $i++;
            }
            for ($index = 1; $index < $i; $index++) {
                $html .= "</div>\n";
            }
            $html .= "</div>\n";
            return $html;
        }

        /**
         * Auxilia na formação do breadcrumb
         * @param Int $n - Recebe o numero do atual da página
         * @param Int $total - Recebe a quantidade total de páginas
         * @param String $string - Recebe o nome da página
         */
        private function setBreadcrumb($n, $total, $string)
        {
            $convert_string_to_url = $this->formatStringToURL($string);
            if($convert_string_to_url == "mapa-do-site")
            {
                $convert_string_to_url = "mapa-site";
            }
            $url_breadcrumb = $this->url . $convert_string_to_url;
            if(!empty($this->url_extension))
            {
                $url_breadcrumb .= $this->url_extension;
            }
            $html  = "<div itemprop=\"child\" itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">\n";
            $html .= "<a href=\"" . $url_breadcrumb . "\" title=\"" . $string . "\" itemprop=\"url\">\n";
            $html .= $n == $total ? "<strong>" : "";
            $html .= "<span class=\"page\" itemprop=\"title\">" . $string . "</span>";
            $html .= $n == $total ? "</strong>" : "";
            $html .= "\n</a>\n";
            return $html;
        }
        
        /**
         * Verifica se a imagem existe
         * @param String $src - Recebe o endereço da imagem
         */
        private function verificaImagem($src)
        {
            $new_img = $this->url . "imagens/thumbs/sem-imagem.jpg";
            if(strpos($this->url, "localhost") == true)
            {
                $file = str_replace($this->url, "", $src);
                if(file_exists($file))
                {
                    $new_img = $src;
                }
            }
            else
            {
                $new_img = $src;
            }
            return $new_img;
        }

        /**
         * Verifica se a imagem existe
         * @param String $palavra - Recebe o nome da palavra chave
         * @param Int $quantidade - Recebe a quantiade de imagens da galeria
         */
        public function listaGaleria($palavra = null, $quantidade = null)
        {
            $html = "";
            try {
                if(empty($palavra))
                {
                    throw new Exception("Palavra chave não definida", 1);
                }
                if(empty($quantidade))
                {
                    throw new Exception("Quantidade não definida", 2);
                }
                $html .= "<div class=\"lista-galeria-fancy row\">";
                $url_palavra = $this->formatStringToURL($palavra);
                $src_default = $this->url."imagens/".$url_palavra."/";
                for ($i = 1; $i <= $quantidade; $i++)
                {
                    $img_big   = $src_default.$url_palavra."-".$i.".jpg";
                    $img_small = $src_default.$url_palavra."-thumb-".$i.".jpg";
                    $html .= "<div class=\"col-xs-4 col-sm-3 col-md-3 col-lg-3\">";
                    $html .= "<a href=\"".$img_big."\" title=\"".$palavra." - ".$i."\" data-fancybox-group=\"".$url_palavra."\">";
                    $html .= "<img src=\"".$img_small."\" alt=\"".$palavra." - ".$i."\" title=\"".$palavra." - ".$i."\" class=\"img-responsive\">";
                    $html .= "</a>";
                    $html .= "</div>";
                }
                $html .= "</div>";
                return $html;
            } catch (Exception $e) {
                switch ($e->getCode())
                {
                    case 1:
                        return "<p style=\"text-align:center\">".$e->getMessage()."</p>";
                        break;
                    case 2:
                        return $html;
                        break;
                    default:
                        break;
                }
            }
        }
        
        public function compressCSS($array_files = null)
        {
            $root_files = $this->assets_dir."css/";
            if(!empty($this->css_files_default))
            {
                foreach($this->css_files_default as $file)
                {
                    $this->css_compress .= (is_file($root_files.$file.".css") ? $this->my_file_get_contents($this->url.$root_files.$file.".css") : "");
                }
            }
            if(isset($array_files) && !empty($array_files))
            {
                foreach($array_files as $file)
                {
                    $this->css_compress .= (is_file($root_files.$file.".css") ? $this->my_file_get_contents($this->url.$root_files.$file.".css") : "");
                }
            }
            $this->css_compress = str_replace(array("\r", "\n"), '', $this->css_compress);
            $this->css_compress = preg_replace_callback('!/\*[^*]*\*+([^/][^*]*\*+)*/!',
            function($m){
                return "";
            }, $this->css_compress);
            $this->css_compress = str_replace('{ ','{', $this->css_compress);
            $this->css_compress = str_replace(' }', '}', $this->css_compress);
            $this->css_compress = str_replace('; ', ';', $this->css_compress);
            $this->css_compress = str_replace(["\r\n","\r","\n","\t",'  ','    ','     '], '', $this->css_compress);
            $this->css_compress = preg_replace_callback(['(( )+{)','({( )+)'],
            function($m){
                return "{";
            }, $this->css_compress);
            $this->css_compress = preg_replace_callback(['(( )+})','(}( )+)','(;( )*})'],
            function($m){
                return "}";
            }, $this->css_compress);
            $this->css_compress = preg_replace_callback(['(;( )+)','(( )+;)'],
            function($m){
                return ";";
            }, $this->css_compress);
            if(!empty($this->css_compress))
            {
                echo "<style>".$this->css_compress."</style>";
            }
        }
        
        public function compressJS($array_files = null)
        {
            $root_files = $this->assets_dir."js/";
            if(!empty($this->js_files_default))
            {
                foreach($this->js_files_default as $file)
                {
                    $this->js_compress .= (is_file($root_files.$file.".js") ? $this->my_file_get_contents($this->url.$root_files.$file.".js") : "");
                }
            }
            if(isset($array_files) && !empty($array_files))
            {
                foreach($array_files as $file)
                {
                    $this->js_compress .= (is_file($root_files.$file.".js") ? $this->my_file_get_contents($this->url.$root_files.$file.".js") : "");
                }
            }
            $this->js_compress = str_replace(array("\r", "\n"), '', $this->js_compress);
            $this->js_compress = str_replace(["\r\n","\r","\t","\n",'  ','    ','     '], '', $this->js_compress);
            $this->js_compress = preg_replace_callback(['(( )+\))','(\)( )+)'],
            function($m){
                return ")";
            }, $this->js_compress);
            $this->js_compress = preg_replace_callback('!/\*.*?\*/!s',
            function($m){
                return "";
            }, $this->js_compress);
            $this->js_compress = preg_replace_callback('/\n\s*\n/',
            function($m){
                return "\n";
            }, $this->js_compress);
            if(!empty($this->js_compress))
            {
                echo "<script>".$this->js_compress."</script>";
            }
        }
        
        private function my_file_get_contents($url)
        {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            ob_start();
            curl_exec($ch);
            curl_close($ch);
            $file_contents = ob_get_contents();
            ob_end_clean();
            return $file_contents;
        }
        
        public function alerta($string)
        {
            header('Content-type: text/html; charset=utf-8');
            echo "<p style=\"text-align:center;font-size:26px;color:red;margin:32px 0px;\">".$string."</p>";
        }
        
        private function checkToken()
        {
            try {
                if(empty($this->token))
                {
                    throw new Exception("Token vazio", 1);
                }
                @$website = simplexml_load_file($this->url_validation_token.$this->token);
                if(empty($website))
                {
                    if(!$this->openDatabaseConnection())
                    {
                        throw new Exception("Não foi possível fazer a conexão com o banco de dados.", 2);
                    }
                    $sql = "SELECT id, dominio, controle_token_id FROM clientes WHERE token = :token AND excluido = 0;";
                    $query = $this->db->prepare($sql);
                    if(!$query->execute(array(":token" => $this->token)))
                    {
                        throw new Exception("Conexão com Token não foi estabelecida.", 3);
                    }
                    $website = $query->fetch(PDO::FETCH_OBJ);
                    $website->status = $website->controle_token_id;
                    if(empty($website))
                    {
                        throw new Exception("Token não encontrado.");
                    }
                }
                if($website->status == 2)
                {
                    $this->tags_status = false;
                }
                if($website->status == 3)
                {
                    die();
                }
                if($website->status == 4)
                {
                    header("location: ".$website->dominio);
                }
            } catch (Exception $e) {
                switch ($e->getCode())
                {
                    case 2:
                        break;
                    default:
                        $this->alerta("Erro de configuração");
                        echo "<p style=\"text-align:center\">".$e->getMessage()."</p>";
                        die();
                        break;
                }
            }
        }
        
        private function openDatabaseConnection()
        {
            $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
            try {
                $this->db = new PDO($this->db_type . ":host=" . $this->db_host . ";dbname=" . $this->db_name . ";charset=utf8", $this->db_user, $this->db_passwd, $options);
                return true;
            } catch (PDOException $e) {
                return false;
            }
        }
        
    }