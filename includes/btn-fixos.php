<div class="btn-fixo">
	<a class="btn-telephone" title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
        <i class="fas fa-phone-alt"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-whatsapp" title="whatsApp" href="https://api.whatsapp.com/send?phone=5511973167229&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es">
       	<i class="fab fa-whatsapp"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-mail" href="https://www.instagram.com/institutodador/">
        <i class="fab fa-instagram"></i>
   	</a>
</div>