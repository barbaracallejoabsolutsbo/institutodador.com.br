<ul>
	<li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
    <li><a href="<?php echo $url; ?>a-clinica" title="A Clínica">A Clínica</a></li>
    <li><a href="<?php echo $url; ?>o-shian" title="O Shian">O Shian</a>
    <li><a href="<?php echo $url; ?>fisioterapia" title="Fisioterapia">Fisioterapia</a>
    <li><a href="<?php echo $url; ?>tratamentos" title="Tratamentos">Tratamentos</a>
        <ul class="sub-menu">
           <li><a href="<?php echo $url; ?>tratamentos-patologicos" title="Tratamentos Patológicos">Tratamentos Patológicos</a></li>
           <li><a href="<?php echo $url; ?>tratamentos-preventivos" title="Tratamentos Preventivos">Tratamentos Preventivos</a></li>
           <li><a href="<?php echo $url; ?>acupuntura" title="Acupuntura">Acupuntura</a></li>
        </ul>
    </li>
    <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a>
        <ul>
            <?php echo $padrao->subMenu($palavras_chave); ?>
        </ul>
    </li>
</ul>