<?php

    // Principais Dados do Cliente
    $nome_empresa = "Instituto da Dor";
    $emailContato = "institutodador@hotmail.com";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "Instituto da Dor",
        "rua" => "Rua Visconde de Itaboraí, 171 ",
        "bairro" => "Tatuapé",
        "cidade" => "São Paulo",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "03308-050",
        "latitude_longitude" => "-23.540985789394995, -46.57289245767072", // Consultar no maps.google.com
        "ddd" => "11",
        "telefone" => "2225-1288",
        "whatsapp" => "97316-7229",
        "link_maps" => "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7315.518886809671!2d-46.572871!3d-23.541153000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5ee9f5581927%3A0x61a06ca0da6875f0!2sR.%20Visc.%20de%20Itabora%C3%AD%2C%20171%20-%20Tatuap%C3%A9%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003308-050!5e0!3m2!1spt-BR!2sbr!4v1628608460253!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
    ),

    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);
    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/institutodador.com.br/",
        // URL online
    "https://www.institutodador.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    // $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    // $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    // $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
     $smtp_contato            = "162.241.2.49";
     $email_remetente         = "dispara-email@absolutsbo.com.br";
     $senha_remetente         = "DisparaAbsolut123?";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
   $palavras_chave = array(
    "Ciático inflamado",
    "Clínica de Acupuntura",
    "Clínica de Quiropraxia",
    "Clínica de fisioterapia",
    "Clínica de liberação miofascial",
    "Dor no pescoço",
    "Dry needling",
    "Formigamento nas pernas",
    "Home care",
    "Laserterapia",
    "Lesão por esforço repetitivo",
    "Liberação miofascial manual",
    "Quiropraxia instrumental",
    "Quiropraxia manual",
    "Tensão muscular",
    "Tratamento ATM",
    "Tratamento Artrite",
    "Tratamento Artrose",
    "Tratamento Bursite",
    "Tratamento cervicalgia",
    "Tratamento cervicobraquialgia",
    "Tratamento ciatalgia",
    "Tratamento condromalácia",
    "Tratamento dor atrás do joelho",
    "Tratamento dor ciático",
    "Tratamento dor na coluna",
    "Tratamento dor nas mãos",
    "Tratamento dor no ombro",
    "Tratamento dor sacral",
    "Tratamento dores na coxa",
    "Tratamento dores na lombar",
    "Tratamento dores nas costas",
    "Tratamento dores no trapézio",
    "Tratamento dores nos joelhos",
    "Tratamento epicondilite",
    "Tratamento para Síndrome de ciclista",
    "Tratamento para Síndrome do túnel do carpo",
    "Tratamento para dor",
    "Tratamento para dor de cabeça",
    "Tratamento para dores de cabeça",
    "Tratamento para escoliose",
    "Tratamento para esporão calcâneo",
    "Tratamento para fascite plantar",
    "Tratamento para fibromialgia",
    "Tratamento para hérnia de disco",
    "Tratamento para síndrome do cotovelo de tenista",
    "Tratamento para síndrome do manguito rotador",
    "Tratamento para síndrome do piriforme",
    "Tratamento para torcicolo",
    "Tratamento tendinite"
    );
       // Listas de Palavras Chave
    $menu_patologico = array(
        "Artrite",
        "Artrose",
        "Bursite",
        "Capsulite articular",
        "Cervicalgia / Cervicobraquialgia",
        "Ciatalgia",
        "Epicondilite",
        "Escoliose",
        "Esporão Calcâneo",
        "Fibromialgia",
        "Hérnia de Disco",
        "Hipercifose",
        "Lombalgia",
        "Manguito Rotador",
        "Tendinite",
        "Túnel do carpo"
    );

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */