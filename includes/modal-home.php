<style>


.modal-info .cover {
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 1;
}

.modal-info .blur-in {
    -webkit-animation: blur 2s forwards;
    -moz-animation: blur 2s forwards;
    -o-animation: blur 2s forwards;
    animation: blur 2s forwards;
}

.modal-info .blur-out {
    -webkit-animation: blur-out 2s forwards;
    -moz-animation: blur-out 2s forwards;
    -o-animation: blur-out 2s forwards;
    animation: blur-out 2s forwards;
}

.modal-info @-webkit-keyframes blur {
    0% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }

    100% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }
}

.modal-info @-moz-keyframes blur {
    0% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }

    100% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }
}

.modal-info @-o-keyframes blur {
    0% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }

    100% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }
}

.modal-info @keyframes blur {
    0% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }

    100% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }
}

.modal-info @-webkit-keyframes blur-out {
    0% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }

    100% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }
}

.modal-info @-moz-keyframes blur-out {
    0% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }

    100% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }
}

.modal-info @-o-keyframes blur-out {
    0% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }

    100% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }
}

.modal-info @keyframes blur-out {
    0% {
        -webkit-filter: blur(4px);
        -moz-filter: blur(4px);
        -o-filter: blur(4px);
        -ms-filter: blur(4px);
        filter: blur(4px);
    }

    100% {
        -webkit-filter: blur(0px);
        -moz-filter: blur(0px);
        -o-filter: blur(0px);
        -ms-filter: blur(0px);
        filter: blur(0px);
    }
}

.modal-info .content {
    width: 650px;
    margin: 0 auto;
    padding-top: 100px;
}

.modal-info span {
    color: dimgray;
}

.modal-info .pop-up {
    position: fixed;
    margin: 0% auto;
    left: 0;
    right: 0;
    z-index: 1000;
    width: 50%;
}

.modal-info .box {
    background-color: whitesmoke;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    margin-top: 14%;
    position: relative;
    -webkit-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
}

.modal-info .button {
    margin 0 auto;
  background-color: #FF8566;
    margin-bottom: 33px;
}

.modal-info .button:hover {
    background-color: #7CCF29;
    -webkit-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.1);
}

.modal-info .close-button {
    transition: all 0.5s ease;
    position: absolute;
    background-color: #FF9980;
    padding: 1.5px 7px;
    left: 0;
    margin-left: -10px;
    margin-top: -9px;
    border-radius: 50%;
    border: 2px solid #fff;
    color: white;
    -webkit-box-shadow: -4px -2px 6px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: -4px -2px 6px 0px rgba(0,0,0,0.1);
    box-shadow: -3px 1px 6px 0px rgba(0,0,0,0.1);
}

.modal-info .close-button:hover {
    background-color: tomato;
    color: #fff;
}

    </style>


<!--<div class="modal-info">
        <div class="row pop-up">
          <div class="box small-6 large-centered">
            <a href="<?php echo $canonical; ?>#" class="close-button">&#10006;</a>
            <img src="<?php echo $url; ?>imagens/info.jpg" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
        </div>
    </div>
</div>-->