<header itemscope itemtype="http://schema.org/Organization">
    <div class="topo">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="logo-info-contato">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="logo">
                        <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                            <span itemprop="image">
                                <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <nav class="menu">
                    <ul class="menu-list">
                        <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                        <li><a href="<?php echo $url; ?>a-clinica" title="Quem somos">Quem somos</a></li>
                        <li><a href="<?php echo $url; ?>tratamentos" title="Tratamentos">Tratamentos</a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo $url; ?>acupuntura" title="Acupuntura">Acupuntura</a></li>
                                <li><a href="<?php echo $url; ?>tratamentos-preventivos" title="Liberação Miofascial">Liberação Miofascial</a></li>
                                <li><a href="<?php echo $url; ?>tratamentos-patologicos" title="Quiropraxia">Quiropraxia</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                        <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                    </ul>
                </nav>
            </div>
                <!-- <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/telefone.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p>
                                <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span></a>
                                <br>
                                <a title="whatsApp" href="https://api.whatsapp.com/send?phone=5511973167229&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es">
                                <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a>
                                <br>
                                <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                                    <span itemprop="telephone"> <?php echo $emailContato; ?></span>
                                </a> 
                            </p> 
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/mapa.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p><?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?></p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            
        </div>
    </div>



    




</header>