<div id="basic-carousel" class="flexslider">
    <ul class="lista-galeria-fancy slides">
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-1.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-1.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-2.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-2.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-3.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-3.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-4.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-4.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-5.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-5.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-6.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-6.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>
        <li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo $url; ?>imagens/empresa/instituto-7.jpg" title="Instituto da Dor" data-fancybox-group="item">
                <img src="<?php echo $url; ?>imagens/empresa/instituto-7.jpg" alt="Instituto da Dor" title="Instituto da Dor" class="img-responsive">
                </a>
            </div>
        </li>

    </ul>
</div>