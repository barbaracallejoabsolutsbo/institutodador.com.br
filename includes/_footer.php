<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="logo text-justify">
                    <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                        <span itemprop="image">
                            <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                        </span>
                    </a>
                    <p>O Instituto da Dor é uma clínica que realiza tratamentos terapêuticos aplicando acupuntura, quiropraxia e a técnica de liberação miofascial exclusiva, visando tratamentos patológicos e preventivos.</p>
                    <br>
                    <p><strong>Responsável técnico: Dr. Caio Liboni Castilho - CREFITO: 142589-F</strong></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h5>Atendimento:</h5>
                <p>Segunda a sexta 08h às 21h</p>
                <p>Sábado 08h às 14h</p>
                <br>
                <h5>Contatos</h5>
                <p><strong>Telefone</strong>:<a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span></a></p>
                <p><strong>WhatsApp</strong>: <a title="whatsApp" href="">
                                <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a></p>
                <p><strong>Endereço</strong>: <?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?></p>

            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <h5>Menu</h5>
                <ul>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>a-clinica" title="Sobre">Quem Somos</a></li>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>tratamentos-patologicos" title="Tratamentos">Tratamentos</a>
                        <ul>
                            <li><a href="<?php echo $url; ?>acupuntura" title="Acupuntura">Acupuntura</a></li>
                            <li><a href="<?php echo $url; ?>tratamentos-preventivos" title="Liberação Miofascial">Liberação Miofascial</a></li>
                            <li><a href="<?php echo $url; ?>tratamentos-patologicos" title="Quiropraxia">Quiropraxia</a></li>
                        </ul>
                    </li>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                    <li><i class="fas fa-check"></i> <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-left">
                    <p class="desenvolvido">Copyright © 2021 <?php echo $nome_empresa; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                    <div class="baixa-absolut"></div>
                    <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                    <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                        <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap">
                    </a>
                    <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="https://api.whatsapp.com/send?phone=5511973167229&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es" class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
</footer>
  <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-XYJ61601ZX"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-XYJ61601ZX');
</script>
        <?php } ?>