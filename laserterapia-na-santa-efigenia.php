<?php
$title       = "Laserterapia na Santa Efigênia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Comumente utilizado na fisioterapia a fim de estimular a recuperação de tecidos musculares, combater inflamações e dores, a Laserterapia na Santa Efigênia trata dores crônicas, artrite reumatóide, osteoartrite, dor nas articulações, dores miofasciais, epicondilite lateral, alterações envolvendo nervos periféricos, e até mesmo para tratamento de nervo ciático, com casos de ótimos resultados. Agende já uma sessão conosco.</p>
<p>Sendo referência no ramo Tratamentos terapêuticos, garante o melhor em Laserterapia na Santa Efigênia, a empresa Instituto da Dor trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Tratamento Dores na Coxa, Tratamento Condromalácia, Quiropraxia Instrumental TIQ, Lesão por Esforço Repetitivo e Quiropraxia Manual para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>