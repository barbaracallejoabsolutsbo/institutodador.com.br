<?php
$title       = "Home Care na Penha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Home Care na Penha é o nome que se dá ao serviço de atendimento e assistência médica domiciliar. Nesse tipo de atendimento, enfermeiros, médicos e especialistas vão até o paciente para atendimento na residência do próprio paciente. Uma ótima alternativa para idosos, deficientes, internação domiciliar, entre outros. Encontre Home Care na Penha com preço acessível perto de você. </p>
<p>Além de sermos uma empresa especializada em Home Care na Penha disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Tratamento Artrose, Tratamento para Esporão Calcâneo, Tratamento para Síndrome do Piriforme, Tratamento Dores no Trapézio e Liberação Miofascial Manual. Com a ampla experiência que a equipe Instituto da Dor possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Tratamentos terapêuticos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>