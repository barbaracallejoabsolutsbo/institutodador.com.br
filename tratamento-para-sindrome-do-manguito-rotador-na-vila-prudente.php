<?php
$title       = "Tratamento para Síndrome do Manguito Rotador na Vila Prudente";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No Instituto da Dor você encontra Tratamento para Síndrome do Manguito Rotador na Vila Prudente com profissionais especializados. Por meio de sessões de fisioterapia visando melhorar a flexibilidade, fortalecimento da musculatura, circulação e reparação tecidual, os ganhos e benefícios em relação a movimentação e redução de dor são altamente satisfatórios. Agende uma consulta para tratamentos e procedimentos.</p>
<p>Desempenhando uma das melhores assessorias do segmento de Tratamentos terapêuticos, a Instituto da Dor se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Tratamento para Síndrome do Manguito Rotador na Vila Prudente com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Lesão por Esforço Repetitivo, Tratamento Tendinite, Tensão Muscular, Tratamento para Escoliose e Liberação Miofascial Manual, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>